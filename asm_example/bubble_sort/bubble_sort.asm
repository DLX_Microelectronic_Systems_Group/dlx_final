; loading unsorted array in the data memory

addi r1, r0, 3
sw 0(r0), r1	; M[0]=3
addi r1, r0, 1	
sw 1(r0), r1	; M[1]=1
addi r1, r0, 10
sw 2(r0), r1	; M[2]=10
addi r1, r0, 5
sw 3(r0), r1	; M[3]=5
addi r1, r0, 23
sw 4(r0), r1	; M[4]=23
addi r1, r0, 7
sw 5(r0), r1	; M[5]=7
addi r1, r0, 3	
sw 6(r0), r1	; M[6]=3
addi r1, r0, 1	
sw 7(r0), r1	; M[7]=1
addi r1, r0, 11
sw 8(r0), r1	; M[8]=11
addi r1, r0, 4
sw 9(r0), r1	; M[9]=4
addi r1, r0, 23
sw 10(r0), r1	; M[10]=23
addi r1, r0, 7
sw 11(r0), r1	; M[11]=7

; bubble sort algorithm

; initialize variables

addi r30, r0, 11	; r30 = n-1 = 11
addi r29, r0, 0		; r29 = i = 0

loop1:

addi r28, r0, 0		; r28 = j = 0
sub r27, r30, r29	; r27 = n-i-1

loop2:

lw r1, 0(r28)		; r1 = M[j]
lw r2, 1(r28)		; r2 = M[j+1]
sgt r3, r1, r2		; if r1>r2 then r3=1
nop
nop
beqz r3, no_swap	; if r3=0 (r1<r2) don't swap the two elements
sw 1(r28), r1		; swapping
sw 0(r28), r2

no_swap:
addi r28, r28, 1	; j++
slt r4, r28, r27	; if j<n-i-1 then r4=1
nop
nop
bnez r4, loop2		; loop until j<n-i-1

addi r29, r29, 1	; i++
slt r5, r29, r30	; if i<n-1 then r5=1
nop
nop
bnez r5, loop1		; loop until i<n-1

end:
j end				; loop forever

