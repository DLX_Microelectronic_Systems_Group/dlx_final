addi r1, r0, 81			; r1 = 81
addi r2, r0, 9			; r2 = 9
xor r3, r3, r3			; r3 = 0
divide:
	slt r5, r1, r2		; if r1<r2 then r5=1
	nop
	nop					; needed because forwarding to the zero detector is not implemented
	bnez r5, finish		; finish if r5!=0 
	sub r1, r1, r2		; else r1=r1-r2
	addi r3, r3, 1		; r3=r3+1
	j divide			; loop
finish:
add r4, r0, r1			; r4 = r1
nop
