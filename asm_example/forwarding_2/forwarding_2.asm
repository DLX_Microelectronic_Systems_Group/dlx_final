addi r6, r0, 1;		r6=1
sw 7(r0), r6;		M[7]=1
addi r7, r0, 2;		r7=2
sw 9(r0), r7;		M[9]=2
addi r8, r0, 3;		r8=3
sw 3(r0), r8;		M[3]=3

addi r1, r0, #7;	r1=7		FDEMW
addi r2, r1, #3;	r2=10		 FDEMW				forward r1 from instruction i-1
sw 10(r1), r2;		M[17]=10	  FDEMW				forward r2 from instruction i-1 and r1 from instruction i-2
sub r3, r1, r2;		r3=-3		   FDEMW			forward r2 from instruction i-2
lw r4, 10(r3);		r4=M[7]=1		FDEMW			forward r3 from instruction i-1
add r1, r2, r3;		r1=7			 FDEMW			forward r3 from instruction i-2
lw r2, 2(r1);		r2=M[9]=2		  FDEMW			forward r1 from instruction i-1
lw r3, 2(r4);		r3=M[3]=3		   FDEMW		
sw 10(r3), r3;		M[10]=3				F-DEMW		stall 1 cycle + forward r3 from instruction i-1
sw 15(r3), r3;		M[15]=3				  FDEMW		forward r3 from instruction i-2 
addi r1, r2, #4;	r1=6				   FDEMW	
