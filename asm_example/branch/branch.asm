addi r1, r0, 2		;	r1 = 2
xor r2, r2, r2		;	r2 = 0

loop:
lw r3, 0(r2)		;	r3 = M[0] = 0
addi r3, r3, 10		;	r3 = 10
sw 15(r2), r3		;	M[15] = r3 = 10
subi r1, r1, 1		;	r1 = r1-1
addi r2, r2, 4		;	r2 = r2+4
nop					;	needed because forwarding to the zero detector is not implemented
bnez r1, loop

addi r4, r0, 65535	;	r4 = 65535
ori r5, r4, 100000	;	r5 = 100000
add r6, r4, r5		;	r6 = r5+r4

end:
j end
