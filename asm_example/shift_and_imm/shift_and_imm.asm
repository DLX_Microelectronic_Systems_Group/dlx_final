addi r1, r0, 42			; r1=0x0000002A
addi r2, r0, 0xAAAAAAAA	; r2=0xFFFFAAAA (the immediate is truncated by the compiler to 16 bits)
or r3, r1, r2			; r3=0xFFFFAAAA
addi r1, r0, 2			; r1=2
sll r4, r3, r1			; r4=r3<<2=0xFFFEAAA8
slli r5, r3, 2			; r5=r3<<2=0xFFFEAAA8
srl r6, r5, r1			; r6=r5>>2=0x3FFFAAAA
srli r7, r4, 2			; r7=r4>>2=0x3FFFAAAA

;r6, r7 must be equal

addi r2, r0, 0xF0000000	; r2=0
ori r3, r0, 0xF0F0F0F0	; r3=0xFFFFF0F0
addi r1, r0, 5			; r1=5
sra r8, r3, r1			; r8=r3>>5=0xFFFFFF87
srai r9, r3, 5			; r9=r3>>5=0xFFFFFF87
