addi r1, r0, #7;	r1=7			FDEMW
addi r2, r0, #1;	r2=1			 FDEMW			
add  r3, r1, r2;	r3=r1+r2=8		  FDEMW			forward r1 from instruction i-1 and r2 from instruction i-2
sw 0(r2), r3;		M[1]=r3=8		   FDEMW		forward r3 from instruction i-1 
sw 10(r2), r3;		M[11]=r3=8			FDEMW		forward r3 from instruction i-2
lw r4, 4(r1);		r4=M[11]=8			 FDEMW		
add r5, r4, r3;		r5=r4+r3=16			  F-DEMW	stall 1 cycle + forward r3 from instruction i-1
add r6, r4, r3;		r6=r4+r3=16				FDEMW	forward r3 from instruction i-2
