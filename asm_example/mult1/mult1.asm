addi r1, r0, 6	; r1=6
addi r2, r0, 7	; r2=7
lw r4, 100(r1)	; r4=M[106]
mult r3, r1, r2	; r3=42
mult r5, r1, r3	; r5=42*6=252
sw 100(r3), r5	; M[142]=252
addi r3, r3, 2	; r3=44

shift:
srli r3, r3, 1	; r3=r3/2
nop
nop
bnez r3, shift

fine:
j fine
