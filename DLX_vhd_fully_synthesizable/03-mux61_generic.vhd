library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.mypackage.all;

entity MUX61 is
	generic (N:integer:=N_bit);
	port (
		A:		IN	std_logic_vector (N-1 downto 0);
		B:		IN	std_logic_vector (N-1 downto 0);
		C:		IN	std_logic_vector (N-1 downto 0);
		D:		IN	std_logic_vector (N-1 downto 0);
		E:		IN	std_logic_vector (N-1 downto 0);
		F:		IN	std_logic_vector (N-1 downto 0);
		SEL:	IN	std_logic_vector (2 downto 0);
		Y:		OUT std_logic_vector (N-1 downto 0));
end MUX61;

architecture BEHAVIORAL of MUX61 is
begin

	mux: process(SEL, A, B, C, D, E, F)
	begin
	    case SEL is
	        when "000" 	=> Y <= A;
	        when "001" 	=> Y <= B;
	        when "010" 	=> Y <= C;
	        when "011" 	=> Y <= D;
	        when "100" 	=> Y <= E;
			when others => Y <= F;
	    end case;
	end process;

end BEHAVIORAL;
