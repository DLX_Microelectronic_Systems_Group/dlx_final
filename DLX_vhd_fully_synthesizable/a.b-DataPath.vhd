--top entity datapath
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all;

entity DataPath is
	Generic(N:integer:= N_bit);
	port(
    	CLK:				IN		std_logic;
		RST:				IN		std_logic;
    	PC_IN:				OUT 	std_logic_vector(N-1 downto 0);
        PC_OUT:				IN		std_logic_vector(N-1 downto 0);
        IR_OUT:				IN		std_logic_vector(INSTRUCTION_WIDTH-1 downto 0);
        CTRL:				IN		std_logic_vector(CTRL_LENGTH-1 downto 0);
		ALU_CONTROL:		IN		std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);
		ALU_DIV_RST:		IN		std_logic;
		ALU_DIV_VALID:		OUT		std_logic;
		NPC1_ENABLE:		IN		std_logic;
		FWD_CTRL:			IN		std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
		BRANCH_OUTCOME:		OUT		std_logic;
		-- external data memory bus
		TO_DATAMEM_ADD: 	OUT		std_logic_vector(mylog2(DATA_MEMORY_DEPTH)-1 downto 0);
		TO_DATAMEM_DATAIN:	OUT		std_logic_vector(N-1 downto 0);
		FROM_DATAMEM_OUT:	IN		std_logic_vector(N-1 downto 0)
	);
end DataPath;

architecture structural of Datapath is

component MUX21_generic
	generic (N:integer:=N_bit);
	port (
		A:		IN	std_logic_vector (N-1 downto 0);
		B:		IN	std_logic_vector (N-1 downto 0);
		SEL:	IN	std_logic;
		Y:		OUT std_logic_vector (N-1 downto 0)
	);
end component;

component MUX41 
	generic (N:integer:=N_bit);
	port (
		A:		IN	std_logic_vector (N-1 downto 0);
		B:		IN	std_logic_vector (N-1 downto 0);
		C:		IN	std_logic_vector (N-1 downto 0);
		D:		IN	std_logic_vector (N-1 downto 0);
		SEL:	IN	std_logic_vector (1 downto 0);
		Y:		OUT std_logic_vector (N-1 downto 0));
end component;

component fd_generic
	generic (N: integer := N_bit);
	port(D:            In      std_logic_vector (N-1 downto 0);
	     CK, RESET,EN: In      std_logic;
         Q:            Out     std_logic_vector (N-1 downto 0));
end component;

component data_memory
	Generic(N:integer:= N_bit;
			ENTRIES:integer:=DATA_MEMORY_DEPTH);
	port ( 
	    RESET: 		IN 	std_logic;
		ENABLE: 	IN 	std_logic;
		RNW: 		IN 	std_logic;	-- read not write
		ADDRESS: 	IN 	std_logic_vector(mylog2(ENTRIES)-1 downto 0);
		DATAIN: 	IN 	std_logic_vector(N-1 downto 0);
		DATAOUT: 	OUT std_logic_vector(N-1 downto 0)
	);
end component;

component register_file
	Generic(N:integer:= N_bit);
	port ( 
	    RESET: 		IN 	std_logic;
		ENABLE: 	IN 	std_logic;
		RD1: 		IN 	std_logic;
		RD2: 		IN 	std_logic;
		WR: 		IN 	std_logic;
		ADD_WR: 	IN 	std_logic_vector(mylog2(N)-1 downto 0);
		ADD_RD1: 	IN 	std_logic_vector(mylog2(N)-1 downto 0);
		ADD_RD2: 	IN 	std_logic_vector(mylog2(N)-1 downto 0);
		DATAIN: 	IN 	std_logic_vector(N-1 downto 0);
	    OUT1: 		OUT std_logic_vector(N-1 downto 0);
		OUT2: 		OUT std_logic_vector(N-1 downto 0)
	);
end component;

component sign_extension
    generic (N : integer := N_bit);
	port (
		A : in std_logic_vector (15 downto 0);
		Y : out std_logic_vector (N-1 downto 0)
	);
end component;

component zero_detector 
    generic (N : integer := N_bit);
	port (
		ENABLE: 	IN 	std_logic;
		NEG: 		IN 	std_logic;
		input :       in std_logic_vector (N-1 downto 0);
		zero_detect : out std_logic
	);
end component;

component pentium4adder 
	generic (Nbit_p : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit_p - 1 downto 0);
		B:	In	std_logic_vector(Nbit_p - 1  downto 0);
        Cin: In	std_logic;
		S:	Out	std_logic_vector(Nbit_p - 1  downto 0));
end component;

component ALU_GENERIC
	generic(N: integer := N_bit);
	port(	A			: IN 	std_logic_vector(N-1 downto 0); 
			B			: IN 	std_logic_vector(N-1 downto 0); 
			FUNC		: IN 	std_logic_vector(4 downto 0);
           	ALU_OUT		: OUT	std_logic_vector(N-1 downto 0);
			EN			: IN 	std_logic;
			CLK			: IN	std_logic;
			DIV_RST		: IN	std_logic;
			DIV_VALID	: OUT	std_logic
	);
end component;

component sign_extension_v2
    generic (N : integer := N_bit);
	port (
		A : in std_logic_vector (25 downto 0);
		Y : out std_logic_vector (N-1 downto 0)
	);
end component;

--signals
signal incremented_pc	: std_logic_vector(N-1 downto 0);
signal PC_IN_i			:	std_logic_vector(N-1 downto 0);
signal rf_add_wr, rd_selected, rd1_out, rd2_out, rd3_out : std_logic_vector(RD_FIELD_LENGTH-1 downto 0);
signal rf_datain : std_logic_vector(N-1 downto 0);
signal npc1, npc2, npc3, npc4, A_out, B_out, ME_ff_out, imm2, A_IN, B_IN, ALU_OUT : std_logic_vector(N-1 downto 0);
signal alu_outp, alu_outp1, alu_outp2, alu_outp3, mem_out, mem_out_ff, mem_out_ff2 : std_logic_vector(N-1 downto 0);
signal fwd_to_mem, FWD_out_A, FWD_out_B, ME_datain_in : std_logic_vector(N-1 downto 0);
signal final_out : std_logic_vector(N-1 downto 0);
signal branch_select,branch_select2, zero_detected : std_logic;

signal rf_out1 : std_logic_vector(N-1 downto 0);
signal rf_out2 : std_logic_vector(N-1 downto 0);

signal extended_imm, extended_imm2, extended_imm_mux, A_out2, FWD_out, B_out2  : std_logic_vector(N-1 downto 0);

--port map
begin

    -------------------------------------------------------------------------------------------------------------------
 
	PC_increment: pentium4adder 
    generic map (Nbit_p => N)
    port map (PC_OUT, PC_INCREMENT_CONSTANT, '0', incremented_pc);
    
    mux_rf_add_wr: MUX21_generic
	generic map(N => RD_FIELD_LENGTH)
	port map (LAST_REGISTER, rd3_out, CTRL(18), rf_add_wr);
    
    mux_rf_datain: MUX21_generic
	generic map(N => N)
	port map (npc4, final_out, CTRL(19), rf_datain);
	
    mux_npc1: MUX21_generic
    generic map(N => N)
    port map (alu_outp1, incremented_pc, branch_select2, PC_IN_i);

    branch_select2 <= CTRL(17) or branch_select;
	BRANCH_OUTCOME <= branch_select2;
    -------------------------------------------------------------------------------------------------------------------

	rf: register_file
    generic map (N => N)
    port map (CTRL(0), CTRL(1), CTRL(2), CTRL(3), CTRL(21), rf_add_wr, IR_OUT(25 downto 21), IR_OUT(20 downto 16), rf_datain, rf_out1, rf_out2);
    
    signextend: sign_extension
    generic map (N => N)
    port map (IR_OUT(15 downto 0), extended_imm);

	signextend2: sign_extension_v2
    generic map (N => N)
    port map (IR_OUT(25 downto 0), extended_imm2);

	mux_imm: MUX21_generic
    generic map(N => N)
    port map (extended_imm2, extended_imm, CTRL(4), extended_imm_mux);
    
	mux_rd: MUX21_generic
    generic map(N => RD_FIELD_LENGTH)
    port map (IR_OUT(20 downto 16), IR_OUT(15 downto 11), CTRL(6), rd_selected);    
    
    npc1_ff: fd_generic
    generic map (N => N)
    port map (PC_IN_i, CLK, RST, NPC1_ENABLE, npc1); 
    
    -------------------------------------------------------------------------------------------------------------------

    npc2_ff: fd_generic
    generic map (N => N)
    port map (NPC1, CLK, RST, CTRL(5), npc2); 
    
    A_ff: fd_generic
    generic map (N => N)
    port map (rf_out1, CLK, RST, CTRL(5), A_out); 
    
    B_ff: fd_generic
    generic map (N => N)
    port map (rf_out2, CLK, RST, CTRL(5), B_out); 
    
    Imm_ff: fd_generic
    generic map (N => N)
    port map (extended_imm_mux, CLK, RST, CTRL(5), imm2); 
    
    Rd1_ff: fd_generic
    generic map (N => RD_FIELD_LENGTH)
    port map (rd_selected, CLK, RST, CTRL(5), rd1_out);
    
    -------------------------------------------------------------------------------------------------------------------
    
    mux_A_sel: MUX21_generic
    generic map(N => N)
    port map (npc2, A_out, CTRL(7), A_out2);

    mux_ALU_FWD_sel_A: MUX41
    generic map(N => N)
    port map (alu_outp1, alu_outp2, mem_out_ff, mem_out_ff2, FWD_CTRL(3 downto 2), FWD_out_A);

    mux_ALU_FWD_sel_B: MUX41
    generic map(N => N)
    port map (alu_outp1, alu_outp2, mem_out_ff, mem_out_ff2, FWD_CTRL(5 downto 4), FWD_out_B);		
		
    mux_B_sel: MUX21_generic
    generic map(N => N)
    port map (B_out, imm2, CTRL(8), B_out2);

	mux_FWD_A: MUX21_generic
    generic map(N => N)
    port map (FWD_out_A, A_out2, FWD_CTRL(0), A_IN);
    
	mux_FWD_B: MUX21_generic
    generic map(N => N)
    port map (FWD_out_B, B_out2, FWD_CTRL(1), B_IN);
    
    -------------------------------------------------------------------------------------------------------------------
    
    Alu: ALU_GENERIC
    generic map(N => N)
    port map (A_IN, B_IN, ALU_CONTROL(4 downto 0), alu_outp, ALU_CONTROL(5), CLK, ALU_DIV_RST, ALU_DIV_VALID);
    
    -------------------------------------------------------------------------------------------------------------------
    
    zero_detect: zero_detector
    generic map(N => N)
    port map(CTRL(10), CTRL(11), A_out, zero_detected);
    
    -------------------------------------------------------------------------------------------------------------------
    
    npc3_ff: fd_generic
    generic map (N => N)
    port map (NPC2, CLK, RST, CTRL(9), npc3); 
    
    branch_taken_ff: fd_generic
    generic map (N => 1) 
    port map (D(0) => zero_detected, CK => CLK, RESET => RST, EN => CTRL(9), Q(0) => branch_select);

    alu_out1_ff: fd_generic
    generic map (N => N)
    port map (alu_outp, CLK, RST, CTRL(9), alu_outp1); 
    
    ME_ff: fd_generic
    generic map (N => N)
    port map (B_out, CLK, RST, CTRL(9), ME_ff_out); 
		
	mux_MEM_FWD_IN_SEL: MUX41
    generic map(N => N)
    port map (mem_out_ff, mem_out_ff2, alu_outp2, alu_outp3, FWD_CTRL(7 downto 6), fwd_to_mem);
		
	mux_MEM_FWD: MUX21_generic
    generic map(N => N)
    port map (fwd_to_mem, ME_ff_out, FWD_CTRL(8), ME_datain_in);
    
    Rd2_ff: fd_generic
    generic map (N => RD_FIELD_LENGTH)
    port map (rd1_out, CLK, RST, CTRL(9), rd2_out);
    
    -------------------------------------------------------------------------------------------------------------------
--	  For what concerns the synthesis, the datamemory is an external component
    
--    DataMemory: data_memory
--    generic map (N => N, ENTRIES => DATA_MEMORY_DEPTH)
--    port map (CTRL(13),  CTRL(14),  CTRL(15), alu_outp1(mylog2(DATA_MEMORY_DEPTH)-1 downto 0),  ME_datain_in, mem_out);
    
	TO_DATAMEM_ADD 		<= alu_outp1(mylog2(DATA_MEMORY_DEPTH)-1 downto 0);
	TO_DATAMEM_DATAIN	<= ME_datain_in;
	mem_out				<= FROM_DATAMEM_OUT;

	PC_IN <= PC_IN_i;
    -------------------------------------------------------------------------------------------------------------------
    
    npc4_ff: fd_generic
    generic map (N => N)
    port map (npc3, CLK, RST, CTRL(16), npc4); 
    
    alu_out2_ff: fd_generic
    generic map (N => N)
    port map (alu_outp1, CLK, RST, CTRL(16), alu_outp2); 

    alu_out3_ff: fd_generic
    generic map (N => N)
    port map (alu_outp2, CLK, RST, '1', alu_outp3);
  
    memout_ff: fd_generic
    generic map (N => N)
    port map (mem_out, CLK, RST, CTRL(16), mem_out_ff);
		
    memout_ff2: fd_generic
    generic map (N => N)
    port map (mem_out_ff, CLK, RST, '1', mem_out_ff2);
    
    Rd3_ff: fd_generic
    generic map (N => RD_FIELD_LENGTH)
    port map (rd2_out, CLK, RST, CTRL(16), rd3_out);
    
    -------------------------------------------------------------------------------------------------------------------
    
    mux_out_sel: MUX21_generic
    generic map(N => N)
    port map (alu_outp2, mem_out_ff, CTRL(20), final_out);
    
    -------------------------------------------------------------------------------------------------------------------
    
    -- CTRL(0) = register file reset
    -- CTRL(1) = register file enable 
    -- CTRL(2) = register file RD1
    -- CTRL(3) = register file RD2 
	-- CTRL(4) = MUX IMMEDIATE
    -- CTRL(5) = ENABLE1
	-- CTRL(6) = RD_SEL (=1 if instruction is I-type)
    -- CTRL(7) = A_SELECT
    -- CTRL(8) = B_SELECT
    -- CTRL(9) = ENABLE2
	-- CTRL(10) = ENABLE ZERO DETECTOR
	-- CTRL(11) = NEGATE ZERO DETECTOR OUTPUT
	-- CTRL(12) = --NOT USED--

    -- CTRL(13) = DATA MEMORY RESET
    -- CTRL(14) = DATA MEMORY ENABLE
    -- CTRL(15) = READ/|WRITE DATAMEMORY
    -- CTRL(16) = ENABLE3
	-- CTRL(17) = FORCE_JUMP
    -- CTRL(18) = mux_SELECTION register file_add_wr
    -- CTRL(19) = mux_SELECTION register file_DATA IN
    -- CTRL(20) = MUX OUT SELECT
    -- CTRL(21) = register file WR

    -- ALU_CONTROL(4 downto 0) = ALU_FUNC
    -- ALU_CONTROL(5) = ALU ENABLE

	-- FWD_CTRL(0) = FWD_A (forward to ALU input A)
    -- FWD_CTRL(1) = FWD_B (forward to ALU input B)
    -- FWD_CTRL(3 downto 2) = FWD_ALU_SEL_A (forward TO ALU from ALU_outp1 if ="00", ALU_outp2 if ="01", mem_out_ff if ="10", mem_out_ff2 if ="11")
    -- FWD_CTRL(5 downto 4) = FWD_ALU_SEL_A (forward TO ALU from ALU_outp1 if ="00", ALU_outp2 if ="01", mem_out_ff if ="10", mem_out_ff2 if ="11")
	-- FWD_CTRL(7 downto 6) = FWD_MEM_SEL (forward TO MEM DATAIN from  mem_out_ff if ="00", mem_out_ff2 if ="01", ALU_outp2 if ="10", ALU_outp3 if ="11")
	-- FWD_CRTL(8) = FWD_DATAIN (forward to memory DATAIN)
		
     -------------------------------------------------------------------------------------------------------------------
    
end structural;
