#######################################################
#                                                     
#  Innovus Command Logging File                     
#  Created on Sun Sep 16 10:08:10 2018                
#                                                     
#######################################################

#@(#)CDS: Innovus v17.11-s080_1 (64bit) 08/04/2017 11:13 (Linux 2.6.18-194.el5)
#@(#)CDS: NanoRoute 17.11-s080_1 NR170721-2155/17_11-UB (database version 2.30, 390.7.1) {superthreading v1.44}
#@(#)CDS: AAE 17.11-s034 (64bit) 08/04/2017 (Linux 2.6.18-194.el5)
#@(#)CDS: CTE 17.11-s053_1 () Aug  1 2017 23:31:41 ( )
#@(#)CDS: SYNTECH 17.11-s012_1 () Jul 21 2017 02:29:12 ( )
#@(#)CDS: CPE v17.11-s095
#@(#)CDS: IQRC/TQRC 16.1.1-s215 (64bit) Thu Jul  6 20:18:10 PDT 2017 (Linux 2.6.18-194.el5)

set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default
suppressMessage ENCEXT-2799
getDrawView
loadWorkspace -name Physical
win
set defHierChar /
set delaycal_input_transition_delay 0.1ps
set fpIsMaxIoHeight 0
set init_gnd_net gnd
set init_mmmc_file Default.view
set init_oa_search_lib {}
set init_pwr_net vdd
set init_verilog P4A.v
set init_lef_file /software/dk/nangate45/lef/NangateOpenCellLibrary.lef
set defHierChar /
set delaycal_input_transition_delay 0.1ps
set fpIsMaxIoHeight 0
set init_gnd_net gnd
set init_mmmc_file Default.view
set init_oa_search_lib {}
set init_pwr_net vdd
set init_verilog DLX.v
set init_lef_file /software/dk/nangate45/lef/NangateOpenCellLibrary.lef
init_design
init_design
init_design
init_design
set defHierChar /
set delaycal_input_transition_delay 0.1ps
set fpIsMaxIoHeight 0
set init_gnd_net gnd
set init_mmmc_file Default.view
set init_oa_search_lib {}
set init_pwr_net vdd
set init_verilog DLX.v
set init_lef_file /software/dk/nangate45/lef/NangateOpenCellLibrary.lef
init_design
set defHierChar /
set delaycal_input_transition_delay 0.1ps
set fpIsMaxIoHeight 0
set init_gnd_net gnd
set init_mmmc_file Default.view
set init_oa_search_lib {}
set init_pwr_net vdd
set init_verilog DLX.v
set init_lef_file /software/dk/nangate45/lef/NangateOpenCellLibrary.lef
init_design
gui_select -rect {241.345 143.228 224.374 215.595}
gui_select -rect {109.100 140.987 100.454 203.747}
getIoFlowFlag
setIoFlowFlag 0
floorPlan -coreMarginsBy die -site FreePDK45_38x28_10R_NP_162NW_34O -r 1.0 0.6 5.0 5.0 5.0 5.0
uiSetTool select
getIoFlowFlag
fit
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
setAddRingMode -ring_target default -extend_over_row 0 -ignore_rows 0 -avoid_short 0 -skip_crossing_trunks none -stacked_via_top_layer metal10 -stacked_via_bottom_layer metal1 -via_using_exact_crossover_size 1 -orthogonal_only true -skip_via_on_pin {  standardcell } -skip_via_on_wire_shape {  noshape }
addRing -nets {vdd gnd} -type core_rings -follow core -layer {top metal9 bottom metal9 left metal10 right metal10} -width {top 0.8 bottom 0.8 left 0.8 right 0.8} -spacing {top 0.8 bottom 0.8 left 0.8 right 0.8} -offset {top 1.8 bottom 1.8 left 1.8 right 1.8} -center 1 -extend_corner {} -threshold 0 -jog_distance 0 -snap_wire_center_to_grid None
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
setAddStripeMode -ignore_block_check false -break_at none -route_over_rows_only false -rows_without_stripes_only false -extend_to_closest_target none -stop_at_last_wire_for_area false -partial_set_thru_domain false -ignore_nondefault_domains false -trim_antenna_back_to_shape none -spacing_type edge_to_edge -spacing_from_block 0 -stripe_min_length 0 -stacked_via_top_layer metal10 -stacked_via_bottom_layer metal1 -via_using_exact_crossover_size false -split_vias false -orthogonal_only true -allow_jog { padcore_ring  block_ring }
addStripe -nets {vdd gnd} -layer metal10 -direction vertical -width 0.8 -spacing 0.8 -set_to_set_distance 20 -start_from left -start_offset 15 -switch_layer_over_obs false -max_same_layer_jog_length 2 -padcore_ring_top_layer_limit metal10 -padcore_ring_bottom_layer_limit metal1 -block_ring_top_layer_limit metal10 -block_ring_bottom_layer_limit metal1 -use_wire_group 0 -snap_wire_center_to_grid None -skip_via_on_pin {  standardcell } -skip_via_on_wire_shape {  noshape }
setSrouteMode -viaConnectToShape { noshape }
sroute -connect { blockPin padPin padRing corePin floatingStripe } -layerChangeRange { metal1(1) metal10(10) } -blockPinTarget { nearestTarget } -padPinPortConnect { allPort oneGeom } -padPinTarget { nearestTarget } -corePinTarget { firstAfterRowEnd } -floatingStripeTarget { blockring padring ring stripe ringpin blockpin followpin } -allowJogging 1 -crossoverViaLayerRange { metal1(1) metal10(10) } -nets { vdd gnd } -allowLayerChange 1 -blockPin useLef -targetViaLayerRange { metal1(1) metal10(10) }
setPlaceMode -prerouteAsObs {1 2 3 4 5 6 7 8}
setPlaceMode -fp false
placeDesign
getPinAssignMode -pinEditInBatch -quiet
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Right -layer 1 -spreadType side -pin {{FROM_DATAMEM_OUT[0]} {FROM_DATAMEM_OUT[1]} {FROM_DATAMEM_OUT[2]} {FROM_DATAMEM_OUT[3]} {FROM_DATAMEM_OUT[4]} {FROM_DATAMEM_OUT[5]} {FROM_DATAMEM_OUT[6]} {FROM_DATAMEM_OUT[7]} {FROM_DATAMEM_OUT[8]} {FROM_DATAMEM_OUT[9]} {FROM_DATAMEM_OUT[10]} {FROM_DATAMEM_OUT[11]} {FROM_DATAMEM_OUT[12]} {FROM_DATAMEM_OUT[13]} {FROM_DATAMEM_OUT[14]} {FROM_DATAMEM_OUT[15]} {FROM_DATAMEM_OUT[16]} {FROM_DATAMEM_OUT[17]} {FROM_DATAMEM_OUT[18]} {FROM_DATAMEM_OUT[19]} {FROM_DATAMEM_OUT[20]} {FROM_DATAMEM_OUT[21]} {FROM_DATAMEM_OUT[22]} {FROM_DATAMEM_OUT[23]} {FROM_DATAMEM_OUT[24]} {FROM_DATAMEM_OUT[25]} {FROM_DATAMEM_OUT[26]} {FROM_DATAMEM_OUT[27]} {FROM_DATAMEM_OUT[28]} {FROM_DATAMEM_OUT[29]} {FROM_DATAMEM_OUT[30]} {FROM_DATAMEM_OUT[31]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Right -layer 1 -spreadType side -pin {{TO_DATAMEM_ADD[0]} {TO_DATAMEM_ADD[1]} {TO_DATAMEM_ADD[2]} {TO_DATAMEM_ADD[3]} {TO_DATAMEM_ADD[4]} {TO_DATAMEM_ADD[5]} {TO_DATAMEM_ADD[6]} {TO_DATAMEM_ADD[7]} {TO_DATAMEM_ADD[8]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Right -layer 1 -spreadType side -pin {{TO_DATAMEM_DATAIN[0]} {TO_DATAMEM_DATAIN[1]} {TO_DATAMEM_DATAIN[2]} {TO_DATAMEM_DATAIN[3]} {TO_DATAMEM_DATAIN[4]} {TO_DATAMEM_DATAIN[5]} {TO_DATAMEM_DATAIN[6]} {TO_DATAMEM_DATAIN[7]} {TO_DATAMEM_DATAIN[8]} {TO_DATAMEM_DATAIN[9]} {TO_DATAMEM_DATAIN[10]} {TO_DATAMEM_DATAIN[11]} {TO_DATAMEM_DATAIN[12]} {TO_DATAMEM_DATAIN[13]} {TO_DATAMEM_DATAIN[14]} {TO_DATAMEM_DATAIN[15]} {TO_DATAMEM_DATAIN[16]} {TO_DATAMEM_DATAIN[17]} {TO_DATAMEM_DATAIN[18]} {TO_DATAMEM_DATAIN[19]} {TO_DATAMEM_DATAIN[20]} {TO_DATAMEM_DATAIN[21]} {TO_DATAMEM_DATAIN[22]} {TO_DATAMEM_DATAIN[23]} {TO_DATAMEM_DATAIN[24]} {TO_DATAMEM_DATAIN[25]} {TO_DATAMEM_DATAIN[26]} {TO_DATAMEM_DATAIN[27]} {TO_DATAMEM_DATAIN[28]} {TO_DATAMEM_DATAIN[29]} {TO_DATAMEM_DATAIN[30]} {TO_DATAMEM_DATAIN[31]}}
setPinAssignMode -pinEditInBatch true
editPin -use TIELOW -fixOverlap 1 -spreadDirection clockwise -side Left -layer 1 -spreadType side -pin {{IRAM_ADDR[0]} {IRAM_ADDR[1]} {IRAM_ADDR[2]} {IRAM_ADDR[3]} {IRAM_ADDR[4]} {IRAM_ADDR[5]} {IRAM_ADDR[6]} {IRAM_ADDR[7]} {IRAM_ADDR[8]} {IRAM_ADDR[9]} {IRAM_ADDR[10]} {IRAM_ADDR[11]} {IRAM_ADDR[12]} {IRAM_ADDR[13]} {IRAM_ADDR[14]} {IRAM_ADDR[15]} {IRAM_ADDR[16]} {IRAM_ADDR[17]} {IRAM_ADDR[18]} {IRAM_ADDR[19]} {IRAM_ADDR[20]} {IRAM_ADDR[21]} {IRAM_ADDR[22]} {IRAM_ADDR[23]} {IRAM_ADDR[24]} {IRAM_ADDR[25]} {IRAM_ADDR[26]} {IRAM_ADDR[27]} {IRAM_ADDR[28]} {IRAM_ADDR[29]} {IRAM_ADDR[30]} {IRAM_ADDR[31]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Left -layer 1 -spreadType side -pin {{IRAM_DOUT[0]} {IRAM_DOUT[1]} {IRAM_DOUT[2]} {IRAM_DOUT[3]} {IRAM_DOUT[4]} {IRAM_DOUT[5]} {IRAM_DOUT[6]} {IRAM_DOUT[7]} {IRAM_DOUT[8]} {IRAM_DOUT[9]} {IRAM_DOUT[10]} {IRAM_DOUT[11]} {IRAM_DOUT[12]} {IRAM_DOUT[13]} {IRAM_DOUT[14]} {IRAM_DOUT[15]} {IRAM_DOUT[16]} {IRAM_DOUT[17]} {IRAM_DOUT[18]} {IRAM_DOUT[19]} {IRAM_DOUT[20]} {IRAM_DOUT[21]} {IRAM_DOUT[22]} {IRAM_DOUT[23]} {IRAM_DOUT[24]} {IRAM_DOUT[25]} {IRAM_DOUT[26]} {IRAM_DOUT[27]} {IRAM_DOUT[28]} {IRAM_DOUT[29]} {IRAM_DOUT[30]} {IRAM_DOUT[31]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Bottom -layer 1 -spreadType center -spacing 0.14 -pin CLK
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Bottom -layer 1 -spreadType center -spacing 0.14 -pin RST
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Top -layer 1 -spreadType center -spacing 0.14 -pin DATAPATH_CTRL_13
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Top -layer 1 -spreadType center -spacing 0.14 -pin DATAPATH_CTRL_14
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Top -layer 1 -spreadType center -spacing 0.14 -pin DATAPATH_CTRL_15
setPinAssignMode -pinEditInBatch true
editPin -pinWidth 0.07 -pinDepth 0.07 -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Top -layer 1 -spreadType center -spacing 0.14 -pin DATAPATH_CTRL_15
setPinAssignMode -pinEditInBatch false
zoomSelected
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postCTS
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postCTS
optDesign -postCTS -hold
getFillerMode -quiet
addFiller -cell FILLCELL_X8 FILLCELL_X4 FILLCELL_X32 FILLCELL_X2 FILLCELL_X16 FILLCELL_X1 -prefix FILLER
setNanoRouteMode -quiet -timingEngine {}
setNanoRouteMode -quiet -routeWithSiPostRouteFix 0
setNanoRouteMode -quiet -drouteStartIteration default
setNanoRouteMode -quiet -routeTopRoutingLayer default
setNanoRouteMode -quiet -routeBottomRoutingLayer default
setNanoRouteMode -quiet -drouteEndIteration default
setNanoRouteMode -quiet -routeWithTimingDriven false
setNanoRouteMode -quiet -routeWithSiDriven false
routeDesign -globalDetail
setAnalysisMode -analysisType onChipVariation
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postRoute
optDesign -postRoute -hold
saveDesign DLX_innovus_design
win
win
set_analysis_view -setup {default} -hold {default}
reset_parasitics
extractRC
rcOut -setload DLX.setload -rc_corner standard
rcOut -setres DLX.setres -rc_corner standard
rcOut -spf DLX.spf -rc_corner standard
rcOut -spef DLX.spef -rc_corner standard
redirect -quiet {set honorDomain [getAnalysisMode -honorClockDomains]} > /dev/null
timeDesign -postRoute -pathReports -drvReports -slackReports -numPaths 50 -prefix DLX_postRoute -outDir timingReports
redirect -quiet {set honorDomain [getAnalysisMode -honorClockDomains]} > /dev/null
timeDesign -postRoute -hold -pathReports -slackReports -numPaths 50 -prefix DLX_postRoute_HOLD -outDir timingReports
get_time_unit
report_timing -machine_readable -max_paths 10000 -max_slack 0.75 -path_exceptions all > top.mtarpt
load_timing_debug_report -name default_report top.mtarpt
verifyConnectivity -type all -error 1000 -warning 50
setVerifyGeometryMode -area { 0 0 0 0 } -minWidth true -minSpacing true -minArea true -sameNet true -short true -overlap true -offRGrid false -offMGrid true -mergedMGridCheck true -minHole true -implantCheck true -minimumCut true -minStep true -viaEnclosure true -antenna false -insuffMetalOverlap true -pinInBlkg false -diffCellViol true -sameCellViol false -padFillerCellsOverlap true -routingBlkgPinOverlap true -routingCellBlkgOverlap true -regRoutingOnly false -stackedViasOnRegNet false -wireExt true -useNonDefaultSpacing false -maxWidth true -maxNonPrefLength -1 -error 1000
verifyGeometry
setVerifyGeometryMode -area { 0 0 0 0 }
reportGateCount -level 5 -limit 100 -outfile DLX.gateCount
saveNetlist DLX_postPlaceRout.v
all_hold_analysis_views 
all_setup_analysis_views 
write_sdf  -ideal_clock_network DLX_post_PlaceRoute.sdf
