###################################################################

# Created by write_sdc on Sat Sep 15 22:54:35 2018

###################################################################
set sdc_version 1.7

set_wire_load_model -name 1K_hvratio_1_4 -library NangateOpenCellLibrary
create_clock [get_ports CLK]  -period 3.7  -waveform {0 1.85}
set_max_delay 3.7  -from [list [get_ports CLK] [get_ports RST] [get_ports                        \
{FROM_DATAMEM_OUT[31]}] [get_ports {FROM_DATAMEM_OUT[30]}] [get_ports          \
{FROM_DATAMEM_OUT[29]}] [get_ports {FROM_DATAMEM_OUT[28]}] [get_ports          \
{FROM_DATAMEM_OUT[27]}] [get_ports {FROM_DATAMEM_OUT[26]}] [get_ports          \
{FROM_DATAMEM_OUT[25]}] [get_ports {FROM_DATAMEM_OUT[24]}] [get_ports          \
{FROM_DATAMEM_OUT[23]}] [get_ports {FROM_DATAMEM_OUT[22]}] [get_ports          \
{FROM_DATAMEM_OUT[21]}] [get_ports {FROM_DATAMEM_OUT[20]}] [get_ports          \
{FROM_DATAMEM_OUT[19]}] [get_ports {FROM_DATAMEM_OUT[18]}] [get_ports          \
{FROM_DATAMEM_OUT[17]}] [get_ports {FROM_DATAMEM_OUT[16]}] [get_ports          \
{FROM_DATAMEM_OUT[15]}] [get_ports {FROM_DATAMEM_OUT[14]}] [get_ports          \
{FROM_DATAMEM_OUT[13]}] [get_ports {FROM_DATAMEM_OUT[12]}] [get_ports          \
{FROM_DATAMEM_OUT[11]}] [get_ports {FROM_DATAMEM_OUT[10]}] [get_ports          \
{FROM_DATAMEM_OUT[9]}] [get_ports {FROM_DATAMEM_OUT[8]}] [get_ports            \
{FROM_DATAMEM_OUT[7]}] [get_ports {FROM_DATAMEM_OUT[6]}] [get_ports            \
{FROM_DATAMEM_OUT[5]}] [get_ports {FROM_DATAMEM_OUT[4]}] [get_ports            \
{FROM_DATAMEM_OUT[3]}] [get_ports {FROM_DATAMEM_OUT[2]}] [get_ports            \
{FROM_DATAMEM_OUT[1]}] [get_ports {FROM_DATAMEM_OUT[0]}] [get_ports            \
{IRAM_DOUT[31]}] [get_ports {IRAM_DOUT[30]}] [get_ports {IRAM_DOUT[29]}]       \
[get_ports {IRAM_DOUT[28]}] [get_ports {IRAM_DOUT[27]}] [get_ports             \
{IRAM_DOUT[26]}] [get_ports {IRAM_DOUT[25]}] [get_ports {IRAM_DOUT[24]}]       \
[get_ports {IRAM_DOUT[23]}] [get_ports {IRAM_DOUT[22]}] [get_ports             \
{IRAM_DOUT[21]}] [get_ports {IRAM_DOUT[20]}] [get_ports {IRAM_DOUT[19]}]       \
[get_ports {IRAM_DOUT[18]}] [get_ports {IRAM_DOUT[17]}] [get_ports             \
{IRAM_DOUT[16]}] [get_ports {IRAM_DOUT[15]}] [get_ports {IRAM_DOUT[14]}]       \
[get_ports {IRAM_DOUT[13]}] [get_ports {IRAM_DOUT[12]}] [get_ports             \
{IRAM_DOUT[11]}] [get_ports {IRAM_DOUT[10]}] [get_ports {IRAM_DOUT[9]}]        \
[get_ports {IRAM_DOUT[8]}] [get_ports {IRAM_DOUT[7]}] [get_ports               \
{IRAM_DOUT[6]}] [get_ports {IRAM_DOUT[5]}] [get_ports {IRAM_DOUT[4]}]          \
[get_ports {IRAM_DOUT[3]}] [get_ports {IRAM_DOUT[2]}] [get_ports               \
{IRAM_DOUT[1]}] [get_ports {IRAM_DOUT[0]}]]  -to [list [get_ports {TO_DATAMEM_ADD[8]}] [get_ports {TO_DATAMEM_ADD[7]}]     \
[get_ports {TO_DATAMEM_ADD[6]}] [get_ports {TO_DATAMEM_ADD[5]}] [get_ports     \
{TO_DATAMEM_ADD[4]}] [get_ports {TO_DATAMEM_ADD[3]}] [get_ports                \
{TO_DATAMEM_ADD[2]}] [get_ports {TO_DATAMEM_ADD[1]}] [get_ports                \
{TO_DATAMEM_ADD[0]}] [get_ports {TO_DATAMEM_DATAIN[31]}] [get_ports            \
{TO_DATAMEM_DATAIN[30]}] [get_ports {TO_DATAMEM_DATAIN[29]}] [get_ports        \
{TO_DATAMEM_DATAIN[28]}] [get_ports {TO_DATAMEM_DATAIN[27]}] [get_ports        \
{TO_DATAMEM_DATAIN[26]}] [get_ports {TO_DATAMEM_DATAIN[25]}] [get_ports        \
{TO_DATAMEM_DATAIN[24]}] [get_ports {TO_DATAMEM_DATAIN[23]}] [get_ports        \
{TO_DATAMEM_DATAIN[22]}] [get_ports {TO_DATAMEM_DATAIN[21]}] [get_ports        \
{TO_DATAMEM_DATAIN[20]}] [get_ports {TO_DATAMEM_DATAIN[19]}] [get_ports        \
{TO_DATAMEM_DATAIN[18]}] [get_ports {TO_DATAMEM_DATAIN[17]}] [get_ports        \
{TO_DATAMEM_DATAIN[16]}] [get_ports {TO_DATAMEM_DATAIN[15]}] [get_ports        \
{TO_DATAMEM_DATAIN[14]}] [get_ports {TO_DATAMEM_DATAIN[13]}] [get_ports        \
{TO_DATAMEM_DATAIN[12]}] [get_ports {TO_DATAMEM_DATAIN[11]}] [get_ports        \
{TO_DATAMEM_DATAIN[10]}] [get_ports {TO_DATAMEM_DATAIN[9]}] [get_ports         \
{TO_DATAMEM_DATAIN[8]}] [get_ports {TO_DATAMEM_DATAIN[7]}] [get_ports          \
{TO_DATAMEM_DATAIN[6]}] [get_ports {TO_DATAMEM_DATAIN[5]}] [get_ports          \
{TO_DATAMEM_DATAIN[4]}] [get_ports {TO_DATAMEM_DATAIN[3]}] [get_ports          \
{TO_DATAMEM_DATAIN[2]}] [get_ports {TO_DATAMEM_DATAIN[1]}] [get_ports          \
{TO_DATAMEM_DATAIN[0]}] [get_ports DATAPATH_CTRL_13] [get_ports                \
DATAPATH_CTRL_14] [get_ports DATAPATH_CTRL_15] [get_ports {IRAM_ADDR[31]}]     \
[get_ports {IRAM_ADDR[30]}] [get_ports {IRAM_ADDR[29]}] [get_ports             \
{IRAM_ADDR[28]}] [get_ports {IRAM_ADDR[27]}] [get_ports {IRAM_ADDR[26]}]       \
[get_ports {IRAM_ADDR[25]}] [get_ports {IRAM_ADDR[24]}] [get_ports             \
{IRAM_ADDR[23]}] [get_ports {IRAM_ADDR[22]}] [get_ports {IRAM_ADDR[21]}]       \
[get_ports {IRAM_ADDR[20]}] [get_ports {IRAM_ADDR[19]}] [get_ports             \
{IRAM_ADDR[18]}] [get_ports {IRAM_ADDR[17]}] [get_ports {IRAM_ADDR[16]}]       \
[get_ports {IRAM_ADDR[15]}] [get_ports {IRAM_ADDR[14]}] [get_ports             \
{IRAM_ADDR[13]}] [get_ports {IRAM_ADDR[12]}] [get_ports {IRAM_ADDR[11]}]       \
[get_ports {IRAM_ADDR[10]}] [get_ports {IRAM_ADDR[9]}] [get_ports              \
{IRAM_ADDR[8]}] [get_ports {IRAM_ADDR[7]}] [get_ports {IRAM_ADDR[6]}]          \
[get_ports {IRAM_ADDR[5]}] [get_ports {IRAM_ADDR[4]}] [get_ports               \
{IRAM_ADDR[3]}] [get_ports {IRAM_ADDR[2]}] [get_ports {IRAM_ADDR[1]}]          \
[get_ports {IRAM_ADDR[0]}]]
