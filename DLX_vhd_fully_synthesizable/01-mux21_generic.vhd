library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.mypackage.all;

entity MUX21_generic is
	generic (N:integer:=N_bit);
	port (
		A:		IN	std_logic_vector (N-1 downto 0);
		B:		IN	std_logic_vector (N-1 downto 0);
		SEL:	IN	std_logic;
		Y:		OUT std_logic_vector (N-1 downto 0)
	);
end MUX21_generic;

architecture BEHAVIORAL of MUX21_generic is

begin
	Y <= A when SEL='1' else B;
end BEHAVIORAL;
