library ieee;
use ieee.std_logic_1164.all;
use work.mypackage.all;

entity DLX_plus_memories is
  generic (N : integer := N_bit);
  port (
    CLK 				: IN std_logic;
    RST 				: IN std_logic        	-- Active High
  );
end DLX_plus_memories;

architecture structural of DLX_plus_memories is

	component DLX 
	  generic (N : integer := N_bit);
	  port (
	    CLK 				: IN std_logic;
	    RST 				: IN std_logic;        	-- Active High
		-- connections to external memories (DATA MEMORY and IRAM)
		-- DATA MEMORY
		TO_DATAMEM_ADD		: OUT		std_logic_vector(mylog2(DATA_MEMORY_DEPTH)-1 downto 0);
		TO_DATAMEM_DATAIN	: OUT		std_logic_vector(N-1 downto 0);
		FROM_DATAMEM_OUT	: IN		std_logic_vector(N-1 downto 0);
		DATAPATH_CTRL_13	: OUT		std_logic;
		DATAPATH_CTRL_14	: OUT		std_logic;
		DATAPATH_CTRL_15	: OUT		std_logic;
		-- IRAM
		IRAM_ADDR 			: OUT  std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);
		IRAM_DOUT 			: IN std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0)
		);
	end component;

	component IRAM
	  generic (
	    RAM_DEPTH : integer := 256;
	    I_SIZE : integer := 32);
	  port (
	    Rst  : in  std_logic;
	    Addr : in  std_logic_vector(I_SIZE - 1 downto 0);
	    Dout : out std_logic_vector(I_SIZE - 1 downto 0)
	    );
	end component;

	component data_memory
		Generic(N:integer:= N_bit;
				ENTRIES:integer:=DATA_MEMORY_DEPTH);
		port ( 
		    RESET: 		IN 	std_logic;
			ENABLE: 	IN 	std_logic;
			RNW: 		IN 	std_logic;	-- read not write
			ADDRESS: 	IN 	std_logic_vector(mylog2(ENTRIES)-1 downto 0);
			DATAIN: 	IN 	std_logic_vector(N-1 downto 0);
			DATAOUT: 	OUT std_logic_vector(N-1 downto 0)
		);
	end component;

	-- DATA MEMORY bus
	signal 	TO_DATAMEM_ADD_i	: std_logic_vector(mylog2(DATA_MEMORY_DEPTH)-1 downto 0);
	signal	TO_DATAMEM_DATAIN_i : std_logic_vector(N-1 downto 0);
	signal	FROM_DATAMEM_OUT_i	: std_logic_vector(N-1 downto 0);
	signal	DATAPATH_CTRL_13_i	: std_logic;
	signal	DATAPATH_CTRL_14_i	: std_logic;
	signal	DATAPATH_CTRL_15_i	: std_logic;
		-- IRAM
	signal	IRAM_ADDR_i			: std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);
	signal	IRAM_DOUT_i			: std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);

begin

	-- DLX instantiation
	DXL_i: DLX 
	port map (
	    CLK 				=> CLK,
	    RST 				=> RST,
		TO_DATAMEM_ADD		=> TO_DATAMEM_ADD_i,
		TO_DATAMEM_DATAIN	=> TO_DATAMEM_DATAIN_i,
		FROM_DATAMEM_OUT	=> FROM_DATAMEM_OUT_i,
		DATAPATH_CTRL_13	=> DATAPATH_CTRL_13_i,
		DATAPATH_CTRL_14	=> DATAPATH_CTRL_14_i,
		DATAPATH_CTRL_15	=> DATAPATH_CTRL_15_i,
		IRAM_ADDR 			=> IRAM_ADDR_i,
		IRAM_DOUT 			=> IRAM_DOUT_i
	);

	-- DATA MEMORY instantiation
	DATA_MEMORY_i: data_memory
	port map (
	    RESET	=> DATAPATH_CTRL_13_i,
		ENABLE	=> DATAPATH_CTRL_14_i,
		RNW 	=> DATAPATH_CTRL_15_i,
		ADDRESS => TO_DATAMEM_ADD_i,
		DATAIN	=> TO_DATAMEM_DATAIN_i,
		DATAOUT	=> FROM_DATAMEM_OUT_i
	);

	-- IRAM instantiation
	IRAM_i: IRAM
	port map (
	    Rst  => RST,
	    Addr => IRAM_ADDR_i,
	    Dout => IRAM_DOUT_i
	);

end structural;
