library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use WORK.mypackage.all;

entity zero_detector is
    generic (N : integer := N_bit);
	port (
		input :       in std_logic_vector (N-1 downto 0);
		EN:           in std_logic;
		zero_detect : out std_logic
	);
end zero_detector;

architecture BEHAVIORAL of zero_detector is
	constant zeros: std_logic_vector (N-1 downto 0) := (others => '0');

begin

	zero : process (EN,input) is
	begin
		if(EN = '1' and input = zeros)then
			zero_detect<= '1';
		else
			zero_detect<= '0';
		end if;
	end process;


end architecture BEHAVIORAL;

