library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all;

entity data_memory is
	Generic(N:integer:= N_bit;
			ENTRIES:integer:=DATA_MEMORY_DEPTH);
	port ( 
	    RESET: 		IN 	std_logic;
		ENABLE: 	IN 	std_logic;
		RNW: 		IN 	std_logic;	-- read not write
		ADDRESS: 	IN 	std_logic_vector(mylog2(ENTRIES)-1 downto 0);
		DATAIN: 	IN 	std_logic_vector(N-1 downto 0);
		DATAOUT: 	OUT std_logic_vector(N-1 downto 0)
	);
end data_memory;

architecture behavioral of data_memory is

	subtype REG_ADDR is natural range 0 to ENTRIES-1; -- using natural type
	type REG_ARRAY is array(REG_ADDR) of std_logic_vector(N-1 downto 0); 
	signal REGISTERS : REG_ARRAY;


begin 

	MEMORY_process: process (RESET, ENABLE, RNW, ADDRESS, DATAIN)
	begin
			if reset='1' then
	            for i in 0 to ENTRIES-1 loop
		            REGISTERS(i) <= (others=>'0');
	            end loop;
	            DATAOUT   <= (others=>'0');
            elsif (ENABLE = '1')then 
	            if (RNW = '0') then												-- write operation
		            REGISTERS(to_integer(unsigned(ADDRESS))) <= DATAIN;
	            end if;
				if (RNW = '1') then												-- read operation
				    DATAOUT <= REGISTERS(to_integer(unsigned(ADDRESS)));
	            end if;
            end if;

	end process;

end behavioral;
