library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.mypackage.all;

entity sign_extension is
    generic (N : integer := N_bit);
	port (
		A : in std_logic_vector (15 downto 0);
		Y : out std_logic_vector (N-1 downto 0)
	);
end sign_extension;

architecture behavioral of sign_extension is
begin
	
	extend : process (A) 
	begin
	    for i in 16 to N-1 loop
	        Y(i) <= A(15);
	    end loop;
		Y(15 downto 0) <= A;
    end process;
    
end behavioral;
