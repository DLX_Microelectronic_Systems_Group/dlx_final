library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.mypackage.all;

entity sign_extension_v2 is
    generic (N : integer := N_bit);
	port (
		A : in std_logic_vector (25 downto 0);
		Y : out std_logic_vector (N-1 downto 0)
	);
end sign_extension_v2;

architecture behavioral of sign_extension_v2 is
begin
	
	extend : process (A) 
	begin
	    for i in 26 to N-1 loop
	        Y(i) <= A(25);
	    end loop;
		Y(25 downto 0) <= A;
    end process;
    
end behavioral;
