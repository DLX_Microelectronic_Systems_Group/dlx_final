library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all;

entity register_file is
	Generic(N:integer:= N_bit);
	port ( 
	    RESET: 		IN 	std_logic;
		ENABLE: 	IN 	std_logic;
		RD1: 		IN 	std_logic;
		RD2: 		IN 	std_logic;
		WR: 		IN 	std_logic;
		ADD_WR: 	IN 	std_logic_vector(mylog2(N)-1 downto 0);
		ADD_RD1: 	IN 	std_logic_vector(mylog2(N)-1 downto 0);
		ADD_RD2: 	IN 	std_logic_vector(mylog2(N)-1 downto 0);
		DATAIN: 	IN 	std_logic_vector(N-1 downto 0);
	    OUT1: 		OUT std_logic_vector(N-1 downto 0);
		OUT2: 		OUT std_logic_vector(N-1 downto 0)
	);
end register_file;

architecture behavioral of register_file is

	subtype REG_ADDR is natural range 0 to 31; -- using natural type
	type REG_ARRAY is array(REG_ADDR) of std_logic_vector(N-1 downto 0); 
	signal REGISTERS : REG_ARRAY;

begin 

	RF_process: process (RESET, ENABLE, RD1, RD2, WR, ADD_WR, ADD_RD1, ADD_RD2, DATAIN)
		begin
			if reset='1' then
	            for i in 0 to 31 loop
		            REGISTERS(i) <= (others=>'0');
	            end loop;
	            OUT1 <= (others=>'0');
	            OUT2 <= (others=>'0');
            elsif (ENABLE = '1') then 
	            if (WR = '1') and (to_integer(unsigned(ADD_WR)) /= 0) then	-- Writing on R0 is not allowed
		            REGISTERS(to_integer(unsigned(ADD_WR))) <= DATAIN;
	            end if;
	            if (RD1 = '1') then
					if (WR = '1' and ADD_WR = ADD_RD1) then 
						OUT1 <= DATAIN;										-- give the new data if a read and a write are performed on the same address
					else
						OUT1 <= REGISTERS(to_integer(unsigned(ADD_RD1)));
					end if;
	            end if;
	            if (RD2 = '1') then
					if (WR = '1' and ADD_WR = ADD_RD2) then 
						OUT2 <= DATAIN;
					else
						OUT2 <= REGISTERS(to_integer(unsigned(ADD_RD2)));
					end if;
	            end if;
            end if;
	end process;

end behavioral;
