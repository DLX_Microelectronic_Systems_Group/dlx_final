library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all;

entity ALU_GENERIC is
	generic(N: integer := N_bit);
	port(	A			: IN 	std_logic_vector(N-1 downto 0); 
			B			: IN 	std_logic_vector(N-1 downto 0); 
			FUNC		: IN 	std_logic_vector(4 downto 0);
           	ALU_OUT		: OUT	std_logic_vector(N-1 downto 0);
			EN			: IN 	std_logic;
			CLK			: IN	std_logic;
			DIV_RST		: IN	std_logic;
			DIV_VALID	: OUT	std_logic
	);
end entity ALU_GENERIC;

architecture ARCHITECTURAL of ALU_GENERIC is

	component COMPARATOR_GENERIC
		generic(N: integer := N_bit);
		port(	A: 		in std_logic_vector(N-1 downto 0); 	-- input for comparison
				B: 		in std_logic_vector(N-1 downto 0); 	-- input for comparison
				FUNC: 	in std_logic_vector(2 downto 0); 	-- function select
				CMP_OUT:out std_logic_vector(N-1 downto 0);
				EN:		in 	std_logic
		);
	end component;

	component LOGICALS_GENERIC
		generic(N: integer := N_bit);
		port(	R1: 	in  std_logic_vector(N-1 downto 0);     -- Input operand
				R2: 	in  std_logic_vector(N-1 downto 0);     -- Input operand
				S0: 	in  std_logic;		 					-- Select signal    0    1    0    1    0    1
				S1: 	in  std_logic;     						-- Select signal    0    1    1    0    1    0
				S2: 	in  std_logic; 							-- Select signal    0    1    1    0    1    0
				S3: 	in  std_logic;		 					-- Select signal    1    0    1    0    0    1
				OUTPUT: out std_logic_vector(N-1 downto 0);   	-- Output           AND  NAND OR   NOR  XOR  XNOR
				EN:		in 	std_logic
		);
	end component;

	component shifter 
		generic (N:integer:=N_bit);
		port (
			IN1:		IN 	std_logic_vector(N-1 downto 0);
			IN2:		IN 	std_logic_vector(mylog2(N)-1 downto 0);	
			SHIFTER_OUT:OUT std_logic_vector(N-1 downto 0);
			FUNC:		IN 	std_logic_vector(2 downto 0);
			EN: 		IN 	std_logic);
	end component;

	component P4A_EN 
		generic (Nbit_p : integer := N_bit);
		Port (	
			A:	In	std_logic_vector(Nbit_p - 1 downto 0);
			B:	In	std_logic_vector(Nbit_p - 1  downto 0);
	        ADD_SUB: In std_logic;							-- when ADD_SUB=1 S=A-B else S=A+B
			S:	Out	std_logic_vector(Nbit_p - 1  downto 0);
			EN: In std_logic);
	end component;

	component BOOTHMUL_EN
		Generic (N:integer:= N_bit/2);
		Port (	
			A:	In	std_logic_vector(N downto 1);
			B:	In 	std_logic_vector(N downto 1);
			P:  Out	std_logic_vector(2*N downto 1);
			EN: In	std_logic);
	end component;

	component Restoring_Division
		Generic (N:integer:= N_bit);
		Port (
			Divisor:	    In	std_logic_vector(N/2-1 downto 0);
			Dividend:		In	std_logic_vector(N-1 downto 0);
			EN,CLK,RESET:	In	std_logic;
			Valid_output:	Out std_logic;
			Rest:			Out	std_logic_vector(N/2-1 downto 0);
			Quotient:		Out	std_logic_vector(N/2-1 downto 0));
	end component;

	component sign_extension
	    generic (N : integer := N_bit);
		port (
			A : in std_logic_vector (15 downto 0);
			Y : out std_logic_vector (N-1 downto 0)
		);
	end component;

	component MUX61
		generic (N:integer:=N_bit);
		port (
			A:		IN	std_logic_vector (N-1 downto 0);
			B:		IN	std_logic_vector (N-1 downto 0);
			C:		IN	std_logic_vector (N-1 downto 0);
			D:		IN	std_logic_vector (N-1 downto 0);
			E:		IN	std_logic_vector (N-1 downto 0);
			F:		IN	std_logic_vector (N-1 downto 0);
			SEL:	IN	std_logic_vector (2 downto 0);
			Y:		OUT std_logic_vector (N-1 downto 0));
	end component;

					      						   -- ENABLE(5) ENABLE(4)	ENABLE(3) ENABLE(2) ENABLE(1) ENABLE(0) 
	signal ENABLE : std_logic_vector (5 downto 0); -- divisor   comparator  logic	  shifter	adder	  multiplier

	-- output signals
	signal COMP_OUT, LOGIC_OUT, SHIFT_OUT, P4A_OUT, MUL_OUT, DIV_OUT_EXTENDED : std_logic_vector(N-1 downto 0);
	signal div_out, div_rest : std_logic_vector(N/2-1 downto 0);

	-- function signals
	signal SUB_ADD_FUNC : std_logic; 	-- P4A function select: subtraction if = 1, addition otherwise.
	signal LOGIC_FUNC : std_logic_vector (3 downto 0);
	signal COMP_FUNC : std_logic_vector (2 downto 0);
	signal SHIFT_FUNC : std_logic_vector (2 downto 0);

	-- output mux select signal
	signal OUT_SEL : std_logic_vector (2 downto 0);

begin

	-- instantiate components
	adder: 		P4A_EN port map (A, B, SUB_ADD_FUNC, P4A_OUT, ENABLE(1));
	multiplier: BOOTHMUL_EN port map (A(N/2-1 downto 0), B(N/2-1 downto 0), MUL_OUT, ENABLE(0));
	logic:		LOGICALS_GENERIC port map (A, B, LOGIC_FUNC(0), LOGIC_FUNC(1), LOGIC_FUNC(2), LOGIC_FUNC(3), LOGIC_OUT, ENABLE(3));
	comparator:	COMPARATOR_GENERIC port map (A, B, COMP_FUNC, COMP_OUT, ENABLE(4));
	lg_shifter: SHIFTER port map (A, B(4 downto 0), SHIFT_OUT, SHIFT_FUNC, ENABLE(2));
	divider:	Restoring_Division port map (B(N/2-1 downto 0), A, ENABLE(5), CLK, DIV_RST, DIV_VALID, div_rest, div_out);

	extend:		sign_extension port map (div_out, DIV_OUT_EXTENDED);

	output_mux:	MUX61 port map (P4A_OUT, MUL_OUT, DIV_OUT_EXTENDED, LOGIC_OUT, COMP_OUT, SHIFT_OUT, OUT_SEL, ALU_OUT);

	ALU_DECODER: process (A,B,EN,FUNC) is
	begin
		if EN = '1' then
			case FUNC is 
				WHEN ALU_ADD =>
					ENABLE <= "000010";
					SUB_ADD_FUNC <= '0';
					OUT_SEL <= "000";
				WHEN ALU_SUB =>
					ENABLE <= "000010";
					SUB_ADD_FUNC <= '1';
					OUT_SEL <= "000";
				WHEN ALU_MUL =>
					ENABLE <= "000001";
					OUT_SEL <= "001";
				WHEN ALU_DIV =>
					ENABLE <= "100000";
					OUT_SEL <= "010";
				WHEN ALU_AND =>
					ENABLE <= "001000";
					LOGIC_FUNC <= LOGIC_AND;
					OUT_SEL <= "011";
				WHEN ALU_NAND =>
					ENABLE <= "001000";
					LOGIC_FUNC <= LOGIC_NAND;
					OUT_SEL <= "011";
				WHEN ALU_OR =>
					ENABLE <= "001000";
					LOGIC_FUNC <= LOGIC_OR;
					OUT_SEL <= "011";
				WHEN ALU_NOR =>
					ENABLE <= "001000";
					LOGIC_FUNC <= LOGIC_NOR;
					OUT_SEL <= "011";
				WHEN ALU_XOR =>
					ENABLE <= "001000";
					LOGIC_FUNC <= LOGIC_XOR;
					OUT_SEL <= "011";
				WHEN ALU_XNOR =>
					ENABLE <= "001000";
					LOGIC_FUNC <= LOGIC_XNOR;
					OUT_SEL <= "011";
				WHEN ALU_SGE =>
					ENABLE <= "010000";
					COMP_FUNC <= COMP_SGE;
					OUT_SEL <= "100";
				WHEN ALU_SLE =>
					ENABLE <= "010000";
					COMP_FUNC <= COMP_SLE;
					OUT_SEL <= "100";
				WHEN ALU_SNE =>
					ENABLE <= "010000";
					COMP_FUNC <= COMP_SNE;
					OUT_SEL <= "100";
				WHEN ALU_SGT =>
					ENABLE <= "010000";
					COMP_FUNC <= COMP_SGT;
					OUT_SEL <= "100";
				WHEN ALU_SLT =>
					ENABLE <= "010000";
					COMP_FUNC <= COMP_SLT;
					OUT_SEL <= "100";
				WHEN ALU_SEQ =>
					ENABLE <= "010000";
					COMP_FUNC <= COMP_SEQ;
					OUT_SEL <= "100";
				WHEN ALU_SHL =>
					ENABLE <= "000100";
					SHIFT_FUNC <= SHIFT_SHL;
					OUT_SEL <= "101";
				WHEN ALU_SHRL =>
					ENABLE <= "000100";
					SHIFT_FUNC <= SHIFT_SHRL;
					OUT_SEL <= "101";
				WHEN ALU_SHRA =>
					ENABLE <= "000100";
					SHIFT_FUNC <= SHIFT_SHRA;
					OUT_SEL <= "101";
				WHEN ALU_ROTL =>
					ENABLE <= "000100";
					SHIFT_FUNC <= SHIFT_ROTL;
					OUT_SEL <= "101";
				WHEN ALU_ROTR =>
					ENABLE <= "000100";
					SHIFT_FUNC <= SHIFT_ROTR;
					OUT_SEL <= "101";
				WHEN ALU_NOP =>
					ENABLE <= (others => '0');
					OUT_SEL <= (others => '0');
				WHEN others =>
					ENABLE <= (others => '0');
					OUT_SEL <= (others => '0');
			end case;
		else 
			ENABLE <= (others => '0');
			OUT_SEL <= (others => '0');
		end if;						
	end process;

end architecture ARCHITECTURAL;
