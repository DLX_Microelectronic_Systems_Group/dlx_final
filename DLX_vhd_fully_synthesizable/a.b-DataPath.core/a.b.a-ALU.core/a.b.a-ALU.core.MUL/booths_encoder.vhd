library IEEE;
use IEEE.std_logic_1164.all;
use WORK.mypackage.all;

entity booths_encoder is
	Port (	
		X:	In	std_logic_vector(2 downto 0); -- X(0) = Xi-1, X(1) = Xi, X(2) = Xi+1
		Y:	Out std_logic_vector(2 downto 0));
end booths_encoder;

architecture BEHAVIORAL of booths_encoder is
begin
	with X select Y <=
		"000" when "000",							-- select 0
		"000" when "111",
		"001" when "001",							-- select A, 4A, 16A ...
		"001" when "010",
		"010" when "011",							-- select 2A, 8A, 32A ...
		"011" when "100",							-- select -2A, -8A, -32A ...
		"100" when others; 	-- ("101" or "110");	-- select -A, -4A, -16A ...
end BEHAVIORAL;
