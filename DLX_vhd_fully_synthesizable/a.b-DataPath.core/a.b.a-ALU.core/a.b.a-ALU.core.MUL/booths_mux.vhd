library IEEE;
use IEEE.std_logic_1164.all;
use WORK.mypackage.all;	

entity booths_mux is
	Generic (N:integer:= N_bit);
	Port (
		X0:	In	std_logic_vector(N downto 1);	-- MUX input signals
		X1:	In	std_logic_vector(N downto 1);
		X2:	In	std_logic_vector(N downto 1);
		X3:	In	std_logic_vector(N downto 1);
		X4:	In	std_logic_vector(N downto 1);
		Sel: In std_logic_vector(2 downto 0);	-- MUX select signal
		Y:	Out std_logic_vector(N downto 1));	-- MUX output
end booths_mux;

architecture BEHAVIORAL of booths_mux is
begin
	with sel select Y <=
    X0 when "000", -- 0000..0 mux_in(1)(i)
    X1 when "001", -- A       mux_in(2)(i) 
    X2 when "010", -- 2A	     mux_in(4)(i)
    X3 when "011",-- -2A     mux_in(5)(i)		
	X4 when others; -- "100"; -A mux_in(3)(i)		
end BEHAVIORAL;
