library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all; 

entity BOOTHMUL_EN is
	Generic (N:integer:= N_bit/2);
	Port (	
		A:	In	std_logic_vector(N downto 1);
		B:	In 	std_logic_vector(N downto 1);
		P:  Out	std_logic_vector(2*N downto 1);
		EN: In	std_logic);
end BOOTHMUL_EN;

architecture behavioral of BOOTHMUL_EN is

	component BOOTHMUL
		Generic (N:integer:= N_bit/2);
		Port (	
			A:	In	std_logic_vector(N downto 1);
			B:	In 	std_logic_vector(N downto 1);
			P:  Out	std_logic_vector(2*N downto 1));
	end component;

	signal A_int, B_int : std_logic_vector(N downto 1);

begin 
	
	en_proc: process (A, B, EN)
	begin
		for i in 1 to N loop
			A_int(i) <= A(i) and EN;
			B_int(i) <= B(i) and EN;
		end loop;
	end process;
		
	mul: BOOTHMUL port map (A_int, B_int, P);
		
end behavioral;
