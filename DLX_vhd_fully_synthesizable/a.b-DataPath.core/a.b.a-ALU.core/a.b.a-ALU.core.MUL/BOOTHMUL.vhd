library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all; 

entity BOOTHMUL is
	Generic (N:integer:= N_bit/2);
	Port (	
		A:	In	std_logic_vector(N downto 1);
		B:	In 	std_logic_vector(N downto 1);
		P:  Out	std_logic_vector(2*N downto 1));
end BOOTHMUL;

architecture mixed of BOOTHMUL is

component booths_encoder
	Port (	
		X:	In	std_logic_vector(2 downto 0); -- X(0) = Xi-1, X(1) = Xi, X(2) = Xi+1
		Y:	Out std_logic_vector(2 downto 0));
end component;

component booths_mux
	Generic (N:integer:= N_bit);
	Port (
		X0:	In	std_logic_vector(N downto 1);	-- MUX input signals
		X1:	In	std_logic_vector(N downto 1);
		X2:	In	std_logic_vector(N downto 1);
		X3:	In	std_logic_vector(N downto 1);
		X4:	In	std_logic_vector(N downto 1);
		Sel: In std_logic_vector(2 downto 0);	-- MUX select signal
		Y:	Out std_logic_vector(N downto 1));	-- MUX output
end component;

component pentium4adder
	generic (Nbit_p : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit_p - 1 downto 0);
		B:	In	std_logic_vector(Nbit_p - 1  downto 0);
        Cin: In	std_logic;
		S:	Out	std_logic_vector(Nbit_p - 1  downto 0));
end component; 

type sumVector is array (N/2 downto 1) of std_logic_vector(2*N downto 1);
type selectVector is array (N/2 downto 1) of std_logic_vector(2 downto 0);  
type mux_input_vector is array (5 downto 1) of sumVector;

signal Sa: sumVector;	-- signals for the output of the leftside MUXs
signal Sb: sumVector;	-- signals for the output of the first rightside MUX and for the SUM outputs
signal sel_s: selectVector; -- signals for the output of the encoders
signal mux_in: mux_input_vector; -- mux_in(1 to 5)(row)(bit);

signal zeroes: std_logic_vector(N downto 1) := (others => '0');
signal B_in: std_logic_vector(2*N downto 0);	-- signal B containing also B(-1)='0'
signal A_in: std_logic_vector(2*N downto 1);

begin
	
	B_in <= zeroes&B&'0';	-- B(0) is what we call B(i) for i=-1 and it is always equal to '0'
	A_in <= zeroes&A;

	-- process that creates all the A combinations for the MUX inputs
	A_values: process (A_in, B_in) 
		variable data_temp,data_temp2 : std_logic_vector(2*N downto 1);
		begin
			for i in 1 to N/2 loop
				mux_in(1)(i) <= (others => '0');	-- first input always zero

				mux_in(2)(i) <= std_logic_vector(SHIFT_LEFT(unsigned(A_in), (i-1)*2)); -- second input A, 4A, 16A, 64A, ...

				data_temp := std_logic_vector(SHIFT_LEFT(unsigned(A_in), (i-1)*2));
				data_temp := not data_temp;
				mux_in(3)(i) <= std_logic_vector(unsigned(data_temp) + unsigned(zeroes&zeroes(N downto 2)&'1')); -- third input -A, -4A, -16A, -64A, ...

				mux_in(4)(i) <= std_logic_vector(SHIFT_LEFT(unsigned(A_in), (i-1)*2+1)); -- fourth input 2A, 8A, 32A, 128A, ...

				data_temp2 := std_logic_vector(SHIFT_LEFT(unsigned(A_in), (i-1)*2+1));
				data_temp2 := not data_temp2;
				mux_in(5)(i) <= std_logic_vector((unsigned(data_temp2) + unsigned(zeroes&zeroes(N downto 2)&'1'))); -- fifth input -2A, -8A, -32A, -128A, ...
			end loop;	
	end process;

	-- generate encoders
	ENCODERS: for i in 1 to N/2 generate
		ENCi: booths_encoder port map (B_in((2*i) downto 2*(i-1)), sel_s(i)); 	-- encoders i+1, i, i-1
	end generate;
		
	-- generate multiplexers
	MUX1: booths_mux generic map (2*N)
		port map (mux_in(1)(1), mux_in(2)(1), mux_in(4)(1), mux_in(5)(1), mux_in(3)(1), sel_s(1), Sb(1));
	MUXES: for i in 2 to N/2 generate
		MUXi: 
		booths_mux generic map (2*N)
		port map (mux_in(1)(i), mux_in(2)(i), mux_in(4)(i), mux_in(5)(i), mux_in(3)(i), sel_s(i), Sa(i-1));
	end generate;

	-- generate adders 
	ADDERS: for i in 1 to N/2-1 generate
		ADDi: pentium4adder generic map (2*N) 
			port map (Sa(i), Sb(i), '0', Sb(i+1));
	end generate;
	
	P <= Sb(N/2); -- assign output

end mixed;
