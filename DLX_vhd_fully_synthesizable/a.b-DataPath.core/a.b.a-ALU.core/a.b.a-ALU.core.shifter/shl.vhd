library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.all;
use WORK.mypackage.all;

entity shl is 
	generic (N:integer:=N_bit);
	port (
		IN1:	IN 	std_logic_vector(N-1 downto 0);			-- input to be shifted
		IN2:	IN 	std_logic_vector(mylog2(N)-1 downto 0);	-- input value of the shift
		SHL_OUT:OUT std_logic_vector(N-1 downto 0);
		EN: 	IN 	std_logic);
end shl;

architecture structural of shl is 
	
	constant LOG2N: integer := mylog2(N);
	type signalvector is array (LOG2N downto 0) of std_logic_vector(N-1 downto 0);
	signal s: signalvector;		-- intermediate and output signals
	signal s2: std_logic_vector(LOG2N-1 downto 0);

	component MUX21
	port (
			A:		In	std_logic;	-- when SEL = '1'
			B:		In	std_logic;	-- when SEL = '0'
			SEL:	In	std_logic;
			Y:		Out std_logic);
	end component;

begin 

	-- generate muxes having left input connected to the output of other muxes
	mux_rows2: for i in 0 to LOG2N-1 generate 
		mux_columns2: for j in 2**i to N-1 generate
			muxes2: MUX21 port map(s(i+1)(j-2**i), s(i+1)(j), s2(i), s(i)(j));
		end generate;
 	end generate;

    -- generate the other muxes
	mux_rows: for i in 0 to LOG2N-1 generate 
		mux_columns: for j in 0 to 2**i-1 generate
			muxes: MUX21 port map('0', s(i+1)(j), s2(i), s(i)(j));
		end generate;
 	end generate;
 	
	-- connect outputs
	out_connect: process (s) 
	begin 
		for i in 0 to N-1 loop
			SHL_OUT(i) <= s(0)(i);
		end loop;
	end process;

	-- connect inputs
	in1_connect: process (IN1, EN) 
	begin 
		for i in 0 to N-1 loop
			s(LOG2N)(i) <= IN1(i) and EN;
		end loop;
	end process;

	in2_connect: process (IN2, EN) 
	begin 
		for i in 0 to LOG2N-1 loop
			s2(i) <= IN2(i) and EN;
		end loop;
	end process;

end structural;
