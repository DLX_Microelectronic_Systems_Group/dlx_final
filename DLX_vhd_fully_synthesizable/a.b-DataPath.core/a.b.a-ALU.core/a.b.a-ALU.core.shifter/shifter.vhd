-- Top entity of logarithmic shifter based on multiplexers

library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.mypackage.all;

entity shifter is 
	generic (N:integer:=N_bit);
	port (
		IN1:		IN 	std_logic_vector(N-1 downto 0);
		IN2:		IN 	std_logic_vector(mylog2(N)-1 downto 0);	
		SHIFTER_OUT:OUT std_logic_vector(N-1 downto 0);
		FUNC:		IN 	std_logic_vector(2 downto 0);
		EN: 		IN 	std_logic);
end shifter;

architecture structural of shifter is 

	component MUX41 is
		generic (N:integer:=N_bit);
		port (
			A:		IN	std_logic_vector (N-1 downto 0);
			B:		IN	std_logic_vector (N-1 downto 0);
			C:		IN	std_logic_vector (N-1 downto 0);
			D:		IN	std_logic_vector (N-1 downto 0);
			SEL:	IN	std_logic_vector (1 downto 0);
			Y:		OUT std_logic_vector (N-1 downto 0));
	end component;

	component shr 
		generic (N:integer:=N_bit);
		port (
			IN1:	IN 	std_logic_vector(N-1 downto 0);			-- input to be shifted
			IN2:	IN 	std_logic_vector(mylog2(N)-1 downto 0);	-- input value of the shift
			AR_LG:  IN 	std_logic;								-- arithmetic/!logic
			SHR_OUT:OUT std_logic_vector(N-1 downto 0);
			EN: 	IN 	std_logic);
	end component;

	component shl 
		generic (N:integer:=N_bit);
		port (
			IN1:	IN 	std_logic_vector(N-1 downto 0);			-- input to be shifted
			IN2:	IN 	std_logic_vector(mylog2(N)-1 downto 0);	-- input value of the shift
			SHL_OUT:OUT std_logic_vector(N-1 downto 0);
			EN: 	IN 	std_logic);
	end component;

-- NOTE: Rotators were initially adopted but as there is no rotate instruction in the DLX architecture they have been removed,
--		 they can be added again by uncommenting the following lines and compiling the rotl.vhd rotr.vhd files.

--	component rotr 
--		generic (N:integer:=N_bit);
--		port (
--			IN1:	IN 	std_logic_vector(N-1 downto 0);			-- input to be rotated
--			IN2:	IN 	std_logic_vector(mylog2(N)-1 downto 0);	-- value of the rotation
--			ROTR_OUT:OUT std_logic_vector(N-1 downto 0);
--			EN:		in std_logic);								-- enable signal
--	end component;
	
--	component rotl
--		generic (N:integer:=N_bit);
--		port (
--			IN1:	IN 	std_logic_vector(N-1 downto 0);			-- input to be shifted
--			IN2:	IN 	std_logic_vector(mylog2(N)-1 downto 0);	-- input value of the shift
--			ROTL_OUT:OUT std_logic_vector(N-1 downto 0);
--			EN: 	IN 	std_logic);
--	end component;

	signal EN_SHR, EN_SHL, EN_ROR, EN_ROL : std_logic;
	signal OUT1, OUT2, OUT3, OUT4 :	std_logic_vector (N-1 downto 0);
	signal OUT_MUX_SEL : std_logic_vector (1 downto 0);
begin 	
																							-- FUNC(2) FUNC(1)
	right_shifter: 	shr port map (IN1, IN2, FUNC(0), OUT1, EN_SHR);							-- 11
	left_shifter: 	shl port map (IN1, IN2, OUT2, EN_SHL);									-- 10
--	right_rotator: 	rotr port map (IN1, IN2, OUT3, EN_ROR);									-- 01
--	left_rotator: 	rotl port map (IN1, IN2, OUT4, EN_ROL);									-- 00
	out_mux:		MUX41 port map (OUT4, OUT3, OUT2, OUT1, OUT_MUX_SEL, SHIFTER_OUT);

	-- FUNC(0) ARITHMETIC/!LOGIC
	-- FUNC(1) RIGHT/!LEFT
	-- FUNC(2) SHIFT/!ROTATE

	OUT_MUX_SEL(0) <= FUNC(1); 
	OUT_MUX_SEL(1) <= FUNC(2);

	EN_SHR <= EN and FUNC(2) and FUNC(1);
	EN_SHL <= EN and FUNC(2) and not FUNC(1);
--	EN_ROR <= EN and not FUNC(2) and FUNC(1);
--	EN_ROL <= EN and not FUNC(2) and not FUNC(1);

end structural;
