library IEEE;
use IEEE.std_logic_1164.all; 

entity MUX21 is
	port (
		A:		IN	std_logic;
		B:		IN	std_logic;
		SEL:	IN	std_logic;
		Y:		OUT std_logic);
end MUX21;

architecture BEHAVIORAL of MUX21 is

begin
	Y <= A when SEL='1' else B;
end BEHAVIORAL;
