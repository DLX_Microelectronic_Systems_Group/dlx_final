library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use WORK.mypackage.all;

entity LOGICALS_GENERIC is
	generic(N: integer := N_bit);
	port(	R1: 	in  std_logic_vector(N-1 downto 0);     -- Input operand
			R2: 	in  std_logic_vector(N-1 downto 0);     -- Input operand
			S0: 	in  std_logic;		 					-- Select signal    0    1    0    1    0    1
			S1: 	in  std_logic;     						-- Select signal    0    1    1    0    1    0
			S2: 	in  std_logic; 							-- Select signal    0    1    1    0    1    0
			S3: 	in  std_logic;		 					-- Select signal    1    0    1    0    0    1
			OUTPUT: out std_logic_vector(N-1 downto 0);   	-- Output           AND  NAND OR   NOR  XOR  XNOR
			EN:		in 	std_logic
	);
end entity LOGICALS_GENERIC;

architecture BEHAVIORAL of LOGICALS_GENERIC is

	signal L0, L1, L2, L3: std_logic_vector(N-1 downto 0);

begin

	BITWISE: process (R1,R2,S0,S1,S2,S3, EN) is
	begin
		for i in 0 to N-1 loop
			L0(i) <= not (EN and S0 and (not R1(i)) and (not R2(i)));
			L1(i) <= not (EN and S1 and (not R1(i)) and      R2(i)) ;
			L2(i) <= not (EN and S2 and      R1(i)  and (not R2(i)));
			L3(i) <= not (EN and S3 and      R1(i)  and      R2(i)) ;
		end loop;
	end process;

	OUTPUT <= not (L0 and L1 and L2 and L3);

end architecture BEHAVIORAL;
