library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all;

entity Restoring_Division is
	Generic (N:integer:= N_bit);
	Port (
		Divisor:	    In	std_logic_vector(N/2-1 downto 0);
		Dividend:		In	std_logic_vector(N-1 downto 0);
		EN,CLK,RESET:	In	std_logic;
		Valid_output:	Out std_logic;
		Rest:			Out	std_logic_vector(N/2-1 downto 0);
		Quotient:		Out	std_logic_vector(N/2-1 downto 0));
end Restoring_Division;

architecture beh of Restoring_Division is

signal Divisor_ff, quotient_ff : std_logic_vector(N_bit-1 downto 0);
signal reminder_ff, debuggg : std_logic_vector(N_bit-1 downto 0);
signal counter : integer  range 0 to N_bit;
signal positive_flag : std_logic;
signal neg_result : std_logic_vector (1 downto 0);
constant zeros:  std_logic_vector (N_bit/2-1 downto 0):= (others => '0');

	begin

		DIV: process (CLK,RESET, EN)
		begin
			if (RESET = '1')then
				
					if(Divisor = zeros ) then
						reminder_ff <= 	(others => '0');
						Divisor_ff <=  (others => '0');
						neg_result <= "00";
						counter <= 0;
					elsif(Dividend(N_bit -1) = '1' and Divisor(N_bit/2-1)= '1')then
						reminder_ff <= 	not(Dividend) + 1;
						Divisor_ff <=  (not(Divisor)+1)&zeros;
						neg_result <= "11";
						counter <= N_bit/2;
					elsif(Dividend(N_bit -1) = '1' and Divisor(N_bit/2-1)= '0')then
						reminder_ff <= 	not(Dividend) + 1;
						Divisor_ff <=  Divisor&zeros;
						neg_result <= "10";
						counter <= N_bit/2;
					elsif(Dividend(N_bit -1) = '0' and Divisor(N_bit/2-1)= '1')then
						reminder_ff <= 	Dividend;
						Divisor_ff <=  (not(Divisor)+1)&zeros;
						neg_result <= "01";
						counter <= N_bit/2;
					else
						reminder_ff <= 	Dividend;
						Divisor_ff <=  Divisor&zeros;
						neg_result <= "00";
						counter <= N_bit/2;
					end if;
			
					Valid_output <= '0';
					quotient_ff <= (others => '0');
					
			elsif(EN = '1')then
				if CLK'event and CLK = '1' then 
					if(counter = 0)then
						C:case neg_result is
					      when "11" => 
							Quotient <= quotient_ff( N_bit/2-1 downto 0);
							Rest <= not(reminder_ff(N_bit-1 downto N_bit/2))+1;
							Valid_output <= '1';
					      when "01" => 
						    Quotient <= not(quotient_ff( N_bit/2-1 downto 0))+1;
							Rest <= reminder_ff( N_bit-1 downto N/2);
							Valid_output <= '1';
					      when "10" => 
						    Quotient <= not(quotient_ff( N_bit/2-1 downto 0))+1;
							Rest <= not(reminder_ff( N_bit-1 downto N/2))+1;
							Valid_output <= '1';
						  when others =>
							Quotient <= quotient_ff( N_bit/2-1 downto 0);
							Rest <= reminder_ff( N_bit-1 downto N_bit/2);
							Valid_output <= '1';
						  end case C;
					else
						debuggg <= std_logic_vector(signed(unsigned(reminder_ff( N_bit-2 downto 0)&'0') - unsigned(Divisor_ff( N_bit-1 downto 0))));
						if(signed(unsigned(reminder_ff( N_bit-2 downto 0)&'0') - unsigned(Divisor_ff( N_bit-1 downto 0))) < 0) then
							reminder_ff <= reminder_ff(N_bit-2 downto 0)&'0';
							quotient_ff(counter - 1) <= '0';
							positive_flag <= '0';
						else
							reminder_ff <= std_logic_vector((unsigned(reminder_ff( N_bit-2 downto 0)&'0') - unsigned(Divisor_ff( N_bit-1 downto 0))));
							quotient_ff(counter - 1) <= '1';
							positive_flag <= '1';
						end if;

					if(signed(unsigned(reminder_ff( N_bit-2 downto 0)&'0') - unsigned(Divisor_ff( N_bit-1 downto 0))) = 0) then
						counter <= 0;					
					else
							counter <= counter - 1;
						end if;
					end if;
				end if;
			end if;				
		end process;


end beh;
