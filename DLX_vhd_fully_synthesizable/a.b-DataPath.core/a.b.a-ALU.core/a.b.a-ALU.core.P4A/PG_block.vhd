library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity PG_block is 
	port( 
	      Pin0, Pin1, Gin0, Gin1 : in std_logic;
	      Pout, Gout: out std_logic); 
end PG_block; 

architecture BEHAVIORAL of PG_block is
	begin

  Pout <= Pin0 and Pin1;
  Gout <= Gin1 or (Gin0 and Pin1);

end BEHAVIORAL;
