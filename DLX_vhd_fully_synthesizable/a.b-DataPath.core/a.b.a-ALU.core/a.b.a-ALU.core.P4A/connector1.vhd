library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity connector1 is 
	port( 
	    in1 : in std_logic;
		out1: out std_logic); 
end connector1; 

architecture BEHAVIORAL of connector1 is
begin

  out1 <= in1;

end BEHAVIORAL;
