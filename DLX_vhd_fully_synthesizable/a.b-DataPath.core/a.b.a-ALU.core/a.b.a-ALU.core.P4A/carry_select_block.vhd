library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use WORK.mypackage.all;

entity carry_select_block is 
	generic (Nbit : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit - 1 downto 0);
		B:	In	std_logic_vector(Nbit - 1  downto 0);
		Cin:	In	std_logic;
		S:	Out	std_logic_vector(Nbit - 1  downto 0));
end carry_select_block; 

architecture STRUCTURAL of carry_select_block is

  signal rca_out0 : std_logic_vector(Nbit - 1  downto 0);
  signal rca_out1 : std_logic_vector(Nbit - 1  downto 0);

  component MUX21_GENERIC
	Generic (N:integer:= N_bit);
	Port (	
		A:	In	std_logic_vector(N-1 downto 0);
		B:	In	std_logic_vector(N-1 downto 0);
		SEL:	In	std_logic;
		Y:	Out std_logic_vector(N-1 downto 0));
  end component;

  component RCA_generic 
	generic (Nbit : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit - 1  downto 0);
		B:	In	std_logic_vector(Nbit - 1  downto 0);
		Ci:	In	std_logic;
		S:	Out	std_logic_vector(Nbit - 1  downto 0);
		Co:	Out	std_logic);
end component; 

begin

  RCA4_0: RCA_generic
	   generic map (Nbit => 4) 
	   port map (A, B, '0', rca_out0, open);

  RCA4_1: RCA_generic
	   generic map (Nbit => 4) 
	   port map (A, B, '1', rca_out1, open);

  MUX: mux21_generic
	   generic map (N => 4) 
	   port map (rca_out1, rca_out0, Cin, S);


end STRUCTURAL;


