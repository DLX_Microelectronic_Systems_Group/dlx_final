library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use WORK.mypackage.all;

entity RCA_generic is 
	generic (Nbit : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit - 1 downto 0);
		B:	In	std_logic_vector(Nbit - 1 downto 0);
		Ci:	In	std_logic;
		S:	Out	std_logic_vector(Nbit - 1 downto 0);
		Co:	Out	std_logic);
end RCA_generic; 

architecture STRUCTURAL of RCA_generic is

	signal Ci_s : std_logic_vector(Nbit downto 0);

	component FA 
		Port (	A:	In	std_logic;
			B:	In	std_logic;
			Ci:	In	std_logic;
			S:	Out	std_logic;
			Co:	Out	std_logic);
	end component; 

begin

  Adder: for i in 0 to Nbit - 1 generate
	F_adders: FA port map(
		A=>A(i),
		B=>B(i),
		Ci=>Ci_s(i),
		Co => Ci_s(i+1),
		S => S(i)
		);
  end generate;

  Ci_s(0) <= Ci;
  Co <= Ci_s(Nbit);

end STRUCTURAL;
