library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use WORK.mypackage.all;

entity sum_generator is 
	generic (Nbit : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit - 1 downto 0);
		B:	In	std_logic_vector(Nbit - 1 downto 0);
		Cin:	In	std_logic_vector((Nbit-1)/4 downto 0);
		S:	Out	std_logic_vector(Nbit - 1 downto 0));
end sum_generator; 

architecture STRUCTURAL of sum_generator is

	component carry_select_block 
		generic (Nbit : integer := N_bit);
		Port (	
			A:	In	std_logic_vector(Nbit - 1 downto 0);
			B:	In	std_logic_vector(Nbit - 1  downto 0);
			Cin:	In	std_logic;
			S:	Out	std_logic_vector(Nbit - 1  downto 0));
	end component; 

begin

  carry_blocks: for i in 0 to Nbit/4 -1 generate -- Instantiate Nbit/4 carry select blocks of 4 bits
  begin
	c_block : carry_select_block
	Port Map ( A(3+4*i downto 4*i), B(3+4*i downto 4*i), Cin(i), S(3+4*i downto 4*i));
  end generate;

end STRUCTURAL;


