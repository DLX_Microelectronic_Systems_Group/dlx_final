library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use WORK.mypackage.all; -- libreria WORK user-defined

entity pentium4adder is 
	generic (Nbit_p : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit_p - 1 downto 0);
		B:	In	std_logic_vector(Nbit_p - 1  downto 0);
        Cin: In	std_logic;
		S:	Out	std_logic_vector(Nbit_p - 1  downto 0));
end pentium4adder; 

architecture STRUCTURAL of pentium4adder is

	signal cg_out : std_logic_vector(Nbit_p/4  downto 0);
	constant Nbit_c  :        integer := 4;

	component carry_generator
	Generic (N:integer:= N_bit);
	Port (	
		A:	In	std_logic_vector(N downto 1);
		B:	In	std_logic_vector(N downto 1);
		Cin: In std_logic;
		C:	Out std_logic_vector((N)/4 downto 0));
	end component;

	component carry_select_block 
		generic (Nbit : integer := N_bit);
		Port (	
			A:	In	std_logic_vector(Nbit - 1 downto 0);
			B:	In	std_logic_vector(Nbit - 1  downto 0);
			Cin:	In	std_logic;
			S:	Out	std_logic_vector(Nbit - 1  downto 0));
	end component;

begin

  CG: carry_generator
	generic map (N => Nbit_p)
	port map (A, B,Cin, cg_out);

  CSB0: carry_select_block 
	generic map (Nbit => 4) 
	port map (A(Nbit_c - 1 downto 0), B(Nbit_c - 1 downto 0), Cin ,S(Nbit_c - 1 downto 0));

  	carry_select_blocks: for i in 2 to Nbit_p/4 generate
		CSB: carry_select_block 
			generic map (Nbit => 4) 
			port map (A(Nbit_c*i - 1 downto Nbit_c*(i-1)), B(Nbit_c*i - 1 downto Nbit_c*(i-1)), cg_out(i-1),S(Nbit_c*i - 1 downto Nbit_c*(i-1)));
	end generate;

end STRUCTURAL;
