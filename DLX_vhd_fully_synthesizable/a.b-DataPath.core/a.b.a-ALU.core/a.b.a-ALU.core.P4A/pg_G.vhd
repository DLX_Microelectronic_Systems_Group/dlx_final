library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity pg_G is 
	port( 
	      A, B, cin : in std_logic;
	      G1: out std_logic); 
end pg_G; 

architecture BEHAVIORAL of pg_G is
	begin
		
  -- Pi <= A xor B;
  -- Gi <= A and B;
  -- G1 <= Pi and cin or Gi;
		
  G1 <= ((A xor B) and cin) or (A and B);
	
end BEHAVIORAL;
