library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity PG_network is 
	port( 
	      A, B : in std_logic;
	      Pi, Gi: out std_logic); 
end PG_network; 

architecture BEHAVIORAL of PG_network is
	begin
  Pi <= A xor B;
  Gi <= A and B;

end BEHAVIORAL;
