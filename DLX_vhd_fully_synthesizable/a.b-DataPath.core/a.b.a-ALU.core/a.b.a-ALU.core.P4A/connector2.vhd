library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity connector2 is 
	port( 
	    in1 : in std_logic;
		out1: out std_logic;
		in2 : in std_logic;
		out2: out std_logic); 
end connector2; 

architecture BEHAVIORAL of connector2 is
begin

  out1 <= in1;
  out2 <= in2;

end BEHAVIORAL;
