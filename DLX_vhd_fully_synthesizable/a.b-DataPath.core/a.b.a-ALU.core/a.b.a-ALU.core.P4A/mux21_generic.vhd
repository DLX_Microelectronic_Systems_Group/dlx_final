library IEEE;
use IEEE.std_logic_1164.all; --  libreria IEEE con definizione tipi standard logic
use WORK.mypackage.all; -- libreria WORK user-defined

entity MUX21_GENERIC is
	Generic (N:integer:= N_bit);
	Port (	
		A:	In	std_logic_vector(N-1 downto 0);
		B:	In	std_logic_vector(N-1 downto 0);
		SEL:	In	std_logic;
		Y:	Out std_logic_vector(N-1 downto 0));
end MUX21_GENERIC;

architecture BEHAVIORAL of MUX21_GENERIC is

begin

	Y <= A when SEL='1' else B;

end BEHAVIORAL;
