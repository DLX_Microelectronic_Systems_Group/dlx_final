library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity G_block is 
	port( 
	      Pin1, Gin0, Gin1 : in std_logic;
	      Gout: out std_logic); 
end G_block; 

architecture BEHAVIORAL of G_block is
begin

  Gout <= Gin1 or (Gin0 and Pin1);

end BEHAVIORAL;
