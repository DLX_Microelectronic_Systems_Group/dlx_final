library IEEE;
use IEEE.std_logic_1164.all; --  libreria IEEE con definizione tipi standard logic
use WORK.mypackage.all; -- user-defined package for log2 function

entity carry_generator is
	Generic (N:integer:= N_bit);
	Port (	
		A:	In	std_logic_vector(N downto 1);
		B:	In	std_logic_vector(N downto 1);
		Cin: In std_logic;
		C:	Out std_logic_vector((N)/4 downto 0));
end carry_generator;

architecture STRUCTURAL of carry_generator is

constant rows: integer := mylog2(N);

type signalvector is array (N+1 downto 1) of std_logic_vector(1 downto 0);
type matrix is array (rows downto 0) of signalvector;

signal pg: signalvector;
signal s: matrix;										-- 3-dimentional matrix s(row)(column)(p/g); p/g=0 -> propagate signal, p/g=1 -> generate signal 
signal C_in: std_logic;							-- internal signal for C0

component PG_network
	port( 
	      A, B : in std_logic;
	      Pi, Gi: out std_logic); 
end component; 

component PG_block 
	port( 
	      Pin0, Pin1, Gin0, Gin1 : in std_logic;
	      Pout, Gout: out std_logic); 
end component; 

component G_block 
	port( 
	      Pin1, Gin0, Gin1 : in std_logic;
	      Gout: out std_logic); 
end component; 

component connector1		-- connector1 and connector2 are dummy blocks used for connecting signals of different rows when no other block is present
	port( 
	    in1 : in std_logic;
		  out1: out std_logic); 
end component; 

component connector2
	port( 
	    in1 : in std_logic;
		out1: out std_logic;
		in2 : in std_logic;
		out2: out std_logic); 
end component; 
	
component pg_G
	port( 
	      A, B, cin : in std_logic;
	      G1: out std_logic); 
end component; 

begin

	pg_G1: pg_G port map (A(1), B(1), C_in, s(0)(1)(1));				-- block for carry in propagation
	pg_networks: for i in 2 to N generate 											-- generate a row of pg_networks
		pg_net: PG_network port map(A(i), B(i), s(0)(i)(0), s(0)(i)(1)); -- 0 propagate, 1 generate															
																--A      B        Pi          Gi
 	end generate;
	
	CONN: connector1 port map (s(2)(4)(1), s(3)(4)(1));

	blocks: for i in 1 to rows generate -- i is the row index
			row2: if i < 4 generate		-- first 3 rows of G_blocks and PG_blocks
		  		G: G_block port map(s(i-1)(2**i)(0), s(i-1)(2**(i-1))(1), s(i-1)(2**i)(1), s(i)(2**i)(1)); 
														--       Pin1,              Gin0,             Gin1             Gout 
				columns1: for j in (2**i+1) to N generate
					PGs1: if j mod (2**(i)) = 0 generate
						PG1: PG_block port map(s(i-1)(j-2**(i-1))(0), s(i-1)(j)(0), s(i-1)(j-2**(i-1))(1), s(i-1)(j)(1), s(i)(j)(0), s(i)(j)(1));
									               --Pin0,                Pin1,                Gin0,              Gin1         Pout,        Gout
						end generate;
					CONN: if i = 3 and j mod 4 = 0 and j mod 8 /= 0 generate
						CONN2: connector2 port map (s(i-1)(j)(0), s(i)(j)(0), s(i-1)(j)(1), s(i)(j)(1));
											 --- 		in1,		 out1,		 in2, 		out2
					end generate;
				end generate;
			end generate; -- if i < 4

			row4: if i > 3 generate	-- rows of G_blocks and PG_blocks for rows > 3
				Gs: for k in 0 to 2**(i-3)-1 generate  -- all G blocks and connectors of 1 bit in the empty spaces
					G1: G_block port map(s(i-1)(2**i-k*4)(0), s(i-1)(2**(i-1))(1), s(i-1)(2**i-k*4)(1), s(i)(2**i-k*4)(1));
										 	             --Pin1,                 Gin0,             Gin1,                   Gout
					CONN: connector1 port map (s(i-1)(2**(i-1)-k*4)(1), s(i)(2**(i-1)-k*4)(1));
											--       in                             out
				end generate; 
     -------------------------------------------------------------------------------------------------------
				columns2: for j in 2**i+1 to N generate
					PGs4: if j mod (2**i) = 0 generate
						PGs5: for k in 0 to 2**(i-3)-1 generate -- all PG blocks having Pin0 Gin0 connected to the row right above them (i-1)
							PG4_0: PG_block port map(s(i-1)(j-2**(i-1))(0), s(i-1)(j-4*k)(0), s(i-1)(j-2**(i-1))(1), s(i-1)(j-4*k)(1), s(i)(j-4*k)(0), s(i)(j-4*k)(1));
													  --Pin0,                   Pin1,                 Gin0,                  Gin1         Pout,        Gout
							CONN: connector2 port map (s(i-1)(j-2**(i-1)-4*k)(0), s(i)(j-2**(i-1)-4*k)(0), s(i-1)(j-2**(i-1)-4*k)(1), s(i)(j-2**(i-1)-4*k)(1));
											 --- 				in1,						 out1,		 				in2, 					out2
						end generate; 
					end generate;
				end generate;
     -----------------------------------------------------------------------------------------------------------
			end generate; -- if i > 3
	end generate;

	C_in <= Cin;
	C(0) <= C_in;

	carrry_connect: process (s) 
	begin 
		for i in 1 to N/4 loop
			C(i) <= s(rows)(4*i)(1);
		end loop;
		C(0) <= C_in;
	end process;
		
end STRUCTURAL;
