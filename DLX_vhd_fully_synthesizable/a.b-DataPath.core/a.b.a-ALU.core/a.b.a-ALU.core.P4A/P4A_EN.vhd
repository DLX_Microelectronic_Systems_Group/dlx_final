library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use WORK.mypackage.all; -- libreria WORK user-defined

entity P4A_EN is 
	generic (Nbit_p : integer := N_bit);
	Port (	
		A:	In	std_logic_vector(Nbit_p - 1 downto 0);
		B:	In	std_logic_vector(Nbit_p - 1  downto 0);
        ADD_SUB: In std_logic;							-- when ADD_SUB=1 S=A-B else S=A+B
		S:	Out	std_logic_vector(Nbit_p - 1  downto 0);
		EN: In std_logic);
end P4A_EN;

architecture behavioral of P4A_EN is

	component pentium4adder
		generic (Nbit_p : integer := N_bit);
		Port (	
			A:	In	std_logic_vector(Nbit_p - 1 downto 0);
			B:	In	std_logic_vector(Nbit_p - 1  downto 0);
	        Cin: In	std_logic;
			S:	Out	std_logic_vector(Nbit_p - 1  downto 0));
	end component; 

	signal A_in, B_in : std_logic_vector(Nbit_p - 1 downto 0);
	signal Cin_in : std_logic;
begin

	add_sub_proc: process (A, B, ADD_SUB, EN)
	begin
		if EN = '1' then
			if ADD_SUB = '1' then	-- subtraction
				B_in <= not B;
				A_in <= A;
				Cin_in <= '1';
			else 					-- addition
				B_in <= B;
				A_in <= A;
				Cin_in <= '0';
			end if;
		else						-- not enabled
			B_in <= (others => '0');
			A_in <= (others => '0');
			Cin_in <= '0';
		end if;
	end process;

	p4a: pentium4adder port map (A_in, B_in, Cin_in, S);

end behavioral;
