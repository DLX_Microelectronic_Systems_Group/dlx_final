library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; -- we need a conversion to unsigned 
use WORK.mypackage.all; -- libreria WORK user-defined

entity TB_P4A is 
end TB_P4A; 

architecture TEST_pentium4adder of TB_P4A is

	component P4A_EN is 
		generic (Nbit_p : integer := N_bit);
		Port (	
			A:	In	std_logic_vector(Nbit_p - 1 downto 0);
			B:	In	std_logic_vector(Nbit_p - 1  downto 0);
	        ADD_SUB: In std_logic;							-- when ADD_SUB=1 S=A-B else S=A+B
			S:	Out	std_logic_vector(Nbit_p - 1  downto 0);
			EN: In std_logic);
	end component;

	signal A, B, S1 : std_logic_vector(N_bit-1 downto 0);
	signal ADD_SUB, EN: std_logic;

Begin
  
  DUT: P4A_EN
	   generic map (Nbit_p => N_bit) 
	   port map (A, B, ADD_SUB, S1, EN);

  EN <= '0', '1' after 5 ns;

  STIMULUS1: process
  begin
    A <= "00000000000000000000000000011111";
    B <= "00000000000000000000000000000111";
	ADD_SUB <= '0';
    wait for 10 ns;
    A <= "00000000000000000000000000011111";
    B <= "00000000000000000000000000000111";
	ADD_SUB <= '1';
    wait for 10 ns;
	A <= "00000000000000000000000000000001";
    B <= "11111111111111111111111111111111";
	ADD_SUB <= '0';
    wait for 10 ns;
	A <= "00000000000000000000000000000001";
    B <= "11111111111111111111111111111111";
	ADD_SUB <= '0';
    wait;
  end process STIMULUS1;

end TEST_pentium4adder;

