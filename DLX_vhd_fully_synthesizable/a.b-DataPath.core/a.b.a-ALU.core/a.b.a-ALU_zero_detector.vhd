library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
--use WORK.mypackage.all;

entity zero_detector is
    generic (N : integer := 64);
	port (
		rs1_data :       in std_logic_vector (N-1 downto 0);
		rs2_data :       in std_logic_vector (N-1 downto 0);
		EN:              in std_logic;
		Cin:             in std_logic;
		zero_detect64 : out std_logic;
		zero_detect32 : out std_logic
	);
end zero_detector;

architecture BEHAVIORAL of zero_detector is

	signal P,Z: std_logic_vector(N-1 downto 0);
	signal K: std_logic_vector(N-2 downto 0);

begin

	p0: process (rs1_data,rs2_data, EN) is
	begin
		P <= rs1_data xor rs2_data;
		K <= not(rs1_data(N-2 downto 0)) and not(rs2_data(N-2 downto 0));
	end process;

	p1: process (P,K) is
	begin
		Z(N-1 downto 1) <= P(N-1 downto 1) xor K;
		Z(0) <= P(0) xor (not(Cin));
	end process;

	zero_detect64 <= '1' when (not(Z) = 0) else '0' ;
	zero_detect32 <= '1' when (not(Z(N/2-1 downto 0)) = 0) else '0' ;


end architecture BEHAVIORAL;

