library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use WORK.mypackage.all;

-- comparator operations --

-- SGE 	(set if greater or equal)	"000"
-- SLE	(set if less or equal)		"001"
-- SNE	(set if not equal)			"010"
-- SGT	(set if greater than)		"011"
-- SLT	(set if less than)			"100"
-- SEQ	(set if equal)				"101"

entity COMPARATOR_GENERIC is
	generic(N: integer := N_bit);
	port(	A: 		in std_logic_vector(N-1 downto 0); 	-- input for comparison
			B: 		in std_logic_vector(N-1 downto 0); 	-- input for comparison
			FUNC: 	in std_logic_vector(2 downto 0); 	-- function select
			CMP_OUT:out std_logic_vector(N-1 downto 0);
			EN:		in 	std_logic
	);
end entity COMPARATOR_GENERIC;

architecture BEHAVIORAL of COMPARATOR_GENERIC is

begin

	COMPARISON: process (A,B,EN,FUNC) is
	begin

		if(EN = '1')then
			case FUNC is 
				WHEN COMP_SGE =>
					if (A >= B) then
						CMP_OUT(N-1 downto 1) <= (others => '0');
						CMP_OUT(0) <= '1';
					else 
						CMP_OUT <= (others => '0');
					end if;
				WHEN COMP_SLE =>
					if (A <= B) then
						CMP_OUT(N-1 downto 1) <= (others => '0');
						CMP_OUT(0) <= '1';
					else 
						CMP_OUT <= (others => '0');
					end if;
				WHEN COMP_SNE =>
					if (A /= B) then
						CMP_OUT(N-1 downto 1) <= (others => '0');
						CMP_OUT(0) <= '1';
					else 
						CMP_OUT <= (others => '0');
					end if;
				WHEN COMP_SGT =>
					if (A > B) then
						CMP_OUT(N-1 downto 1) <= (others => '0');
						CMP_OUT(0) <= '1';
					else 
						CMP_OUT <= (others => '0');
					end if;
				WHEN COMP_SLT =>
					if (A < B) then
						CMP_OUT(N-1 downto 1) <= (others => '0');
						CMP_OUT(0) <= '1';
					else 
						CMP_OUT <= (others => '0');
					end if;
				WHEN others =>	
					if (A = B) then
						CMP_OUT(N-1 downto 1) <= (others => '0');
						CMP_OUT(0) <= '1';
					else 
						CMP_OUT <= (others => '0');
					end if;	
			end case;							
		else	-- not enabled
			CMP_OUT <= (others => '0');
		end if;
	end process;

end architecture BEHAVIORAL;
