library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use WORK.mypackage.all;

entity zero_detector is
    generic (N : integer := N_bit);
	port (
		ENABLE: 	IN 	std_logic;
		NEG: 		IN 	std_logic;
		input :       in std_logic_vector (N-1 downto 0);
		zero_detect : out std_logic
	);
end zero_detector;

architecture BEHAVIORAL of zero_detector is

begin

	zero : process (input, ENABLE) is
		variable not_zero : std_logic;
	begin
		if (ENABLE = '1')then 
			not_zero := '0';
			for i in 0 to N-1 loop
				not_zero := not_zero or input(i);
			end loop;

			if(NEG = '0')then
				zero_detect <= not not_zero;
			else
				zero_detect <= not_zero;
			end if;
		else
			zero_detect <= '0';
		end if;
		
	end process;

end architecture BEHAVIORAL;
