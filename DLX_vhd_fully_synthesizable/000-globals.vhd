library IEEE;
use IEEE.std_logic_1164.all; 

package mypackage is

	-----------------
	--- CONSTANTS ---
	-----------------

	constant N_bit 					: integer := 32;
	constant RF_DEPTH 				: integer := 32;
	constant DATA_MEMORY_DEPTH		: integer := 300;
	constant INSTRUCTION_WIDTH 		: integer := 32;
	constant RD_FIELD_LENGTH 		: integer := 5;
	constant IMM_FIELD_LENGTH 		: integer := 16;
	constant CTRL_LENGTH 			: integer := 22;	-- datapath control word length
	constant CU_CTRL_LENGTH 		: integer := 25;	-- control unit control word length (includes fetch stage external to the Datapath and additional RF_EN during the WB stage)
	constant ALU_CTRL_LENGTH 		: integer := 6;
	constant FWD_CTRL_LENGTH		: integer := 9;
	constant PC_INCREMENT_CONSTANT 	: std_logic_vector(N_bit-1 downto 0):= "00000000000000000000000000000100"; -- 4
	constant LAST_REGISTER 			: std_logic_vector(RD_FIELD_LENGTH-1 downto 0) := "11111"; -- 31
	constant OPCODE_LENGTH 			: integer := 6;
	constant FUNC_LENGTH 			: integer := 11;
	
	-- ALU CONSTANTS -----------------------------------------------------------------------

	-- comparator functions
	constant COMP_SGE : std_logic_vector(2 downto 0) := "000";
	constant COMP_SLE : std_logic_vector(2 downto 0) := "001";
	constant COMP_SNE : std_logic_vector(2 downto 0) := "010";
	constant COMP_SGT : std_logic_vector(2 downto 0) := "011";
	constant COMP_SLT : std_logic_vector(2 downto 0) := "100";
	constant COMP_SEQ : std_logic_vector(2 downto 0) := "101";

	-- shifter functions
	constant SHIFT_SHL  : std_logic_vector(2 downto 0) := "100";
	constant SHIFT_SHRL : std_logic_vector(2 downto 0) := "110";
	constant SHIFT_SHRA : std_logic_vector(2 downto 0) := "111";
	constant SHIFT_ROTL : std_logic_vector(2 downto 0) := "000";
	constant SHIFT_ROTR : std_logic_vector(2 downto 0) := "010";

	-- logic unit functions (S3 S2 S1 S0)
	constant LOGIC_AND  : 	std_logic_vector(3 downto 0) := "1000";
	constant LOGIC_NAND : 	std_logic_vector(3 downto 0) := "0111";
	constant LOGIC_OR : 	std_logic_vector(3 downto 0) := "1110";
	constant LOGIC_NOR : 	std_logic_vector(3 downto 0) := "0001";
	constant LOGIC_XOR : 	std_logic_vector(3 downto 0) := "0110";
	constant LOGIC_XNOR : 	std_logic_vector(3 downto 0) := "1001";

	-- ALU functions

	constant ALU_ADD : 	std_logic_vector(4 downto 0) := "00000";
	constant ALU_SUB : 	std_logic_vector(4 downto 0) := "00001";
	constant ALU_MUL : 	std_logic_vector(4 downto 0) := "00010";
	constant ALU_DIV : 	std_logic_vector(4 downto 0) := "00011";
	constant ALU_AND : 	std_logic_vector(4 downto 0) := "00100";
	constant ALU_NAND :	std_logic_vector(4 downto 0) := "00101";	
	constant ALU_OR : 	std_logic_vector(4 downto 0) := "00110";
	constant ALU_NOR : 	std_logic_vector(4 downto 0) := "00111";
	constant ALU_XOR : 	std_logic_vector(4 downto 0) := "01000";
	constant ALU_XNOR :	std_logic_vector(4 downto 0) := "01001";
	constant ALU_SGE :	std_logic_vector(4 downto 0) := "01010";
	constant ALU_SLE :	std_logic_vector(4 downto 0) := "01011";
	constant ALU_SNE :	std_logic_vector(4 downto 0) := "01100";
	constant ALU_SGT :	std_logic_vector(4 downto 0) := "01101";
	constant ALU_SLT :	std_logic_vector(4 downto 0) := "01110";
	constant ALU_SEQ :	std_logic_vector(4 downto 0) := "01111";
	constant ALU_SHL :	std_logic_vector(4 downto 0) := "10000";
	constant ALU_SHRL :	std_logic_vector(4 downto 0) := "10001";
	constant ALU_SHRA : std_logic_vector(4 downto 0) := "10010";
	constant ALU_ROTL : std_logic_vector(4 downto 0) := "10011"; -- NOTE: NOT USED as there is no rotate instruction in the DLX architecture
	constant ALU_ROTR : std_logic_vector(4 downto 0) := "10100"; -- NOTE: NOT USED as there is no rotate instruction in the DLX architecture
	constant ALU_NOP :	std_logic_vector(4 downto 0) := "10101";

	-- CONTROL UNIT WORD MASKS ----------------------------------------------------------------

	constant CU_NPC1_EN 			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000000000001";	-- stage 1  			
	constant CU_IR_EN 				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000000000010";	-- stage 1 

	constant CU_RF_RESET			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000000000100";	-- stage 2 => CTRL(0)
	constant CU_RF_EN_D				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000000001000";	-- stage 2 => CTRL(1)
	constant CU_RF_RD1				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000000010000";	-- stage 2 => CTRL(2)
	constant CU_RF_RD2				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000000100000";	-- stage 2 => CTRL(3)
	constant CU_IMM_SEL				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000001000000";	-- stage 2 => CTRL(4)
	constant CU_EN1					: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000010000000";	-- stage 2 => CTRL(5)
	constant CU_RD_SEL				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000000100000000";	-- stage 2 => CTRL(6)

	constant CU_A_SEL				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000001000000000";	-- stage 3 => CTRL(7)
	constant CU_B_SEL				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000010000000000";	-- stage 3 => CTRL(8)
	constant CU_EN2					: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000000100000000000";	-- stage 3 => CTRL(9)
    constant CU_BRANCH_EN			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000001000000000000";	-- stage 3 => CTRL(10)
	constant CU_BRANCH_NZ			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0010000000010000000000000";	-- stage 3 => CTRL(11)
    constant CU_JUMP_MUX_SEL		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000000100000000000000";	-- stage 3 => CTRL(12)
	
	constant CU_DATA_M_RESET		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000001000000000000000";	-- stage 4 => CTRL(13)
	constant CU_DATA_M_EN			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000010000000000000000";	-- stage 4 => CTRL(14)
	constant CU_DATA_M_RNW			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000000100000000000000000";	-- stage 4 => CTRL(15)
	constant CU_EN3					: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000001000000000000000000";	-- stage 4 => CTRL(16)
    constant CU_FORCE_JUMP			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000010000000000000000000";	-- stage 4 => CTRL(17)

	constant CU_RF_MUX_ADD_WR_SEL	: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0000100000000000000000000";	-- stage 5 => CTRL(18)
	constant CU_RF_MUX_DATAIN_SEL	: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0001000000000000000000000";	-- stage 5 => CTRL(19)
	constant CU_OUT_SEL				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0010000000000000000000000";	-- stage 5 => CTRL(20)
	constant CU_RF_EN_WB			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "0100000000000000000000000";	-- stage 5 => CTRL(1)
	constant CU_RF_WR_WB			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := "1000000000000000000000000";	-- stage 5 => CTRL(21)

	constant CU_FWD_A					: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000000001";
	constant CU_FWD_B					: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000000010";
	constant CU_FWD_ALU_SEL_A_ALUOUT1	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000000000";
    constant CU_FWD_ALU_SEL_A_ALUOUT2	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000000100";
	constant CU_FWD_ALU_SEL_A_MEMOUT1	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000001000";
	constant CU_FWD_ALU_SEL_A_MEMOUT2	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000001100";
	constant CU_FWD_ALU_SEL_B_ALUOUT1	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000000000";
    constant CU_FWD_ALU_SEL_B_ALUOUT2	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000010000";
	constant CU_FWD_ALU_SEL_B_MEMOUT1	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000100000";
	constant CU_FWD_ALU_SEL_B_MEMOUT2	: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000110000";
	constant CU_FWD_MEM_SEL_MEMOUT1		: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "000000000";
	constant CU_FWD_MEM_SEL_MEMOUT2		: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "001000000";
	constant CU_FWD_MEM_SEL_ALUOUT2		: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "010000000";
	constant CU_FWD_MEM_SEL_ALUOUT3		: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "011000000";
	constant CU_FWD_DATAIN				: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0) := "100000000";

	--- list of possible control words for each stage, built starting from the masks above ---
	--- the final control word will be a combination of the following words ------------------

	-- fetch stage control words
	constant CU_FETCH				: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_NPC1_EN or CU_IR_EN;

	-- decode stage control words
	constant CU_READ_RS1			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_D or CU_RF_RD1 or CU_EN1;
	constant CU_READ_RS2			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_D or CU_RF_RD2 or CU_EN1;
	constant CU_READ_RS1_RS2		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_D or CU_RF_RD1 or CU_RF_RD2 or CU_EN1;
	constant CU_READ_IMM			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_EN1;
	constant CU_READ_IMM26			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_IMM_SEL or CU_EN1;
	-- CU_RD_SEL is also useful in this stage

	-- execution stage control words
	constant CU_USE_NPC_AND_B		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_A_SEL or CU_B_SEL or CU_EN2;
	constant CU_USE_A_AND_B			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_B_SEL or CU_EN2;
	constant CU_USE_NPC_AND_IMM		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_A_SEL or CU_EN2;
	constant CU_USE_A_AND_IMM		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_EN2;
	constant CU_BZ 					: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_BRANCH_EN;
    constant CU_BNZ					: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_BRANCH_EN or CU_BRANCH_NZ;
    -- CU_JUMP_MUX_SEL also useful
    
	-- memory stage control words
	constant CU_WRITE_MEM			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_DATA_M_EN or CU_EN3;
	constant CU_READ_MEM			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_DATA_M_EN or CU_DATA_M_RNW or CU_EN3;
	-- CU_EN3, CU_FORCE_JUMP ares also usefu in this stage
    
	-- write back stage control words
	constant CU_WRITE_RF_ADD31		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_WB or CU_RF_WR_WB	or CU_RF_MUX_ADD_WR_SEL; -- write on RF at address 31
	constant CU_WRITE_RF_ADDRD		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_WB or CU_RF_WR_WB;	-- write on RF at address specified by RD
	constant CU_WRITE_RF_PC			: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_WB or CU_RF_WR_WB or CU_RF_MUX_DATAIN_SEL;	-- write PC+4 on RF
	constant CU_WRITE_RF_OUT		: std_logic_vector(CU_CTRL_LENGTH-1 downto 0) := CU_RF_EN_WB or CU_RF_WR_WB;	-- write ALU or MEMORY output on RF
	-- CU_OUT_SEL is also used on this stage

	-- INSTRUCTIONS -----------------------------------------------------------------------------------

	-- INSTRUCTION OPCODE FIELDS
	constant OPCODE_R_TYPE			: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "000000";
	constant OPCODE_F_TYPE			: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "000001";
	constant OPCODE_I_TYPE_ADD		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "001000";
	constant OPCODE_I_TYPE_AND		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "001100";
	constant OPCODE_I_TYPE_OR		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "001101";
	constant OPCODE_I_TYPE_SGE		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "011101";
	constant OPCODE_I_TYPE_SLE		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "011100";
	constant OPCODE_I_TYPE_SLL		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "010100";
	constant OPCODE_I_TYPE_SNE		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "011001";
	constant OPCODE_I_TYPE_SRL		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "010110";
	constant OPCODE_I_TYPE_SUB		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "001010";
	constant OPCODE_I_TYPE_XOR		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "001110";
	constant OPCODE_I_TYPE_SW		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "101011";
	constant OPCODE_I_TYPE_LW		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "100011";
	constant OPCODE_I_TYPE_BEQZ		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "000100";
	constant OPCODE_I_TYPE_BNEZ		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "000101";
	constant OPCODE_I_TYPE_SRA		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "010111";
	constant OPCODE_I_TYPE_SEQ		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "011000";
	constant OPCODE_I_TYPE_SLT		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "011010";
	constant OPCODE_I_TYPE_SGT		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "011011";
	constant OPCODE_J_TYPE_J		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "000010";
	constant OPCODE_J_TYPE_JAL		: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "000011";

	constant OPCODE_NOP				: std_logic_vector(OPCODE_LENGTH-1 downto 0) := "010101";

	-- INSTRUCTION FUNCTION FIELDS
	constant FUNC_ADD				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000100000";
	constant FUNC_AND				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000100100";
	constant FUNC_OR				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000100101";
	constant FUNC_SGE				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000101101";
	constant FUNC_SLE				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000101100";
	constant FUNC_SLL				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000000100";
	constant FUNC_SNE				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000101001";
	constant FUNC_SRL				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000000110";
	constant FUNC_SUB				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000100010";
	constant FUNC_XOR				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000100110";

	constant FUNC_SRA				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000000111";
	constant FUNC_SEQ				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000101000";
	constant FUNC_SLT				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000101010";
	constant FUNC_SGT				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000101011";
	constant FUNC_MULT				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000001110";
	constant FUNC_DIV				: std_logic_vector(FUNC_LENGTH-1 downto 0) := "00000001111";

	--------------------------------------------------------------------------------------------
	--- FUNCTIONS ------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------

	function mylog2( i : natural) return integer;

end mypackage;

package body mypackage is
  function mylog2( i : natural) return integer is
    variable temp    : integer := i;
    variable ret_val : integer := 0; 
		variable flag		 : integer := 1;
  begin					
    while temp > 1 loop
      ret_val := ret_val + 1;
      temp    := temp / 2;  
    end loop;
			if i > 2 ** ret_val then
            return ( ret_val + 1 ) ; 
        else
            return ( ret_val ) ; 
        end if ;
    return ret_val;
	end function;
end mypackage;
