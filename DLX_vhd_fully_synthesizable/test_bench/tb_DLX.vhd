library IEEE;
use IEEE.std_logic_1164.all;
use WORK.mypackage.all;

entity tb_dlx is
end tb_dlx;

architecture test of tb_dlx is

	component DLX_plus_memories
	  generic (N : integer := N_bit);
	  port (
	    CLK 				: IN std_logic;
	    RST 				: IN std_logic        	-- Active High
	  );
	end component;

	signal CLK : std_logic := '0';
	signal RST : std_logic;

begin

	DUT: DLX_plus_memories port map (CLK, RST);

	clock : process (CLK)
	begin
		CLK <= not CLK after 1 ns;
	end process;

	RST <= '1', '0' after 2 ns;

end test;
