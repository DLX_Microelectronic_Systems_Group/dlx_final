vcom ../000-globals.vhd
vcom ../01-mux21_generic.vhd
vcom ../02-mux41_generic.vhd
vcom ../03-mux61_generic.vhd
vcom ../04-fd_generic.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.shifter/mux21.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.shifter/shl.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.shifter/shr.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.shifter/shifter.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.logicals.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/fa.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/rca_generic.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/mux21_generic.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/carry_select_block.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/sum_generator.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/connector1.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/connector2.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/G_block.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/PG.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/PG_block.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/pg_G.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/PG_network.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/carry_generator.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/pentium4adder.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.P4A/P4A_EN.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.MUL/booths_mux.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.MUL/booths_encoder.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.MUL/BOOTHMUL.vhd
vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.MUL/BOOTHMUL_EN.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.comparator.vhd

vcom ../a.b-DataPath.core/a.b.b-sign_extension.vhd
vcom ../a.b-DataPath.core/a.b.b-sign_extension_v2.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.restoring_division.vhd

vcom ../a.b-DataPath.core/a.b.a-ALU.core/a.b.a-ALU.core.top_entity.vhd

vcom ../a.b-DataPath.core/a.b.b-registerfile.vhd
vcom ../a.b-DataPath.core/a.b.b-data_memory.vhd
vcom ../a.b-DataPath.core/a.b.b-zero_detector.vhd

vcom ../a.b-DataPath.vhd
vcom ../a.a-CU_HW.vhd
vcom ../a.c-IRAM.vhd

vcom ../a-DLX.vhd
vcom ../DLX_plus_memories.vhd

vcom tb_DLX.vhd

vsim -t 10ps -novopt work.tb_dlx(test) 

add wave sim:/tb_dlx/clk
add wave sim:/tb_dlx/rst
add wave sim:/tb_dlx/dut/dxl_i/ir
add wave sim:/tb_dlx/clk
add wave sim:/tb_dlx/rst
add wave sim:/tb_dlx/dut/dxl_i/ir
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/reset
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/enable
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/rd1
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/rd2
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/wr
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/add_wr
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/add_rd1
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/add_rd2
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/datain
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/out1
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/out2
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/rf/registers
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/alu/a
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/alu/b
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/alu/func
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/alu/alu_out
add wave sim:/tb_dlx/dut/dxl_i/datapath_i/alu/en
add wave sim:/tb_dlx/dut/dxl_i/pc
add wave sim:/tb_dlx/dut/data_memory_i/reset
add wave sim:/tb_dlx/dut/data_memory_i/enable
add wave sim:/tb_dlx/dut/data_memory_i/rnw
add wave sim:/tb_dlx/dut/data_memory_i/address
add wave sim:/tb_dlx/dut/data_memory_i/datain
add wave sim:/tb_dlx/dut/data_memory_i/dataout
add wave sim:/tb_dlx/dut/data_memory_i/registers

run 40 ns
