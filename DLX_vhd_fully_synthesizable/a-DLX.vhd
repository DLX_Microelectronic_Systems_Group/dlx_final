library ieee;
use ieee.std_logic_1164.all;
use work.mypackage.all;

entity DLX is
  generic (N : integer := N_bit);
  port (
    CLK 				: IN std_logic;
    RST 				: IN std_logic;        	-- Active High
	-- connections to external memories (DATA MEMORY and IRAM)
	-- DATA MEMORY
	TO_DATAMEM_ADD		: OUT		std_logic_vector(mylog2(DATA_MEMORY_DEPTH)-1 downto 0);
	TO_DATAMEM_DATAIN	: OUT		std_logic_vector(N-1 downto 0);
	FROM_DATAMEM_OUT	: IN		std_logic_vector(N-1 downto 0);
	DATAPATH_CTRL_13	: OUT		std_logic;
	DATAPATH_CTRL_14	: OUT		std_logic;
	DATAPATH_CTRL_15	: OUT		std_logic;
	-- IRAM
	IRAM_ADDR 			: OUT  std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);
	IRAM_DOUT 			: IN std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0)
	);
end DLX;

architecture dlx_rtl of DLX is

 --------------------------------------------------------------------
 -- Components Declaration
 --------------------------------------------------------------------

  -- Datapath 
  component DataPath
	Generic(N:integer:= N_bit);
	port(
    	CLK:				IN		std_logic;
		RST:				IN		std_logic;
    	PC_IN:				OUT 	std_logic_vector(N-1 downto 0);
        PC_OUT:				IN		std_logic_vector(N-1 downto 0);
        IR_OUT:				IN		std_logic_vector(INSTRUCTION_WIDTH-1 downto 0);
        CTRL:				IN		std_logic_vector(CTRL_LENGTH-1 downto 0);
		ALU_CONTROL:		IN		std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);
		ALU_DIV_RST:		IN		std_logic;
		ALU_DIV_VALID:		OUT		std_logic;
		NPC1_ENABLE:		IN		std_logic;
		FWD_CTRL:			IN		std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
		BRANCH_OUTCOME:		OUT		std_logic;
		-- external data memory bus
		TO_DATAMEM_ADD: 	OUT		std_logic_vector(mylog2(DATA_MEMORY_DEPTH)-1 downto 0);
		TO_DATAMEM_DATAIN:	OUT		std_logic_vector(N-1 downto 0);
		FROM_DATAMEM_OUT:	IN		std_logic_vector(N-1 downto 0)
	);
	end component;
  
  component DLX_CU
	port (
		CLK				: IN  std_logic;  													-- Clock
		RST				: IN  std_logic;  													-- Reset:Active-High
		IR			  	: IN  std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);   			-- Instruction Register
		BRANCH_SEL		: IN std_logic;
		ALU_CTRL	  	: OUT std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);					-- ALU Function select + ALU enable
		ALU_DIV_RST		: OUT std_logic;													-- divisor reset signal
		ALU_DIV_VALID	: IN std_logic;														-- divisor output ready signal
		DATAPATH_CTRL  	: OUT std_logic_vector(CTRL_LENGTH-1 downto 0); 					-- Datapath control signals (ALU excluded)
		FWD_CTRL_S		: OUT std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
		NPC1_EN			: OUT std_logic;
		IR_EN			: OUT std_logic;
		PC_EN			: OUT std_logic);
  end component;

  ----------------------------------------------------------------
  -- Signals Declaration
  ----------------------------------------------------------------
  
  -- Instruction Register (IR) and Program Counter (PC) declaration
  signal IR : std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);
  signal PC : std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);

  -- Datapath Bus signals
  signal PC_BUS, PC_SHIFTED		: std_logic_vector(INSTRUCTION_WIDTH -1 downto 0);
  signal CTRL_in 				: std_logic_vector(CTRL_LENGTH-1 downto 0);
  signal ALU_CONTROL_in 		: std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);
  signal NPC1_EN_in 			: std_logic;
  signal BRANCH_OUTCOME_in		: std_logic;
  signal FWD_CTRL_in 			: std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);

  -- Data Ram Bus signals
  signal IR_LATCH_EN_i : std_logic;
  signal PC_LATCH_EN_i, ALU_DIV_RST_in, ALU_DIV_VALID_in : std_logic;

  begin  -- DLX

    -- Data Path Instantiation
    Datapath_I:  DataPath
      port map (
        CLK					=>	CLK, 	
		RST					=>	RST,
		PC_IN				=>	PC_BUS,
        PC_OUT				=>	PC,
        IR_OUT				=>	IR,
        CTRL				=>	CTRL_in,
        ALU_CONTROL 		=>	ALU_CONTROL_in,
		ALU_DIV_RST			=>	ALU_DIV_RST_in,
		ALU_DIV_VALID		=>	ALU_DIV_VALID_in,
        NPC1_ENABLE			=>	NPC1_EN_in,
		FWD_CTRL    		=>  FWD_CTRL_in,
		BRANCH_OUTCOME 		=> 	BRANCH_OUTCOME_in,
		TO_DATAMEM_ADD		=>	TO_DATAMEM_ADD,
		TO_DATAMEM_DATAIN	=>	TO_DATAMEM_DATAIN,
		FROM_DATAMEM_OUT	=>	FROM_DATAMEM_OUT
        );

    CU_I: DLX_CU
      port map (
        CLK				=>	CLK, 	
		RST				=>	RST,
		IR				=>  IRAM_DOUT,
		BRANCH_SEL		=> 	BRANCH_OUTCOME_in,
		ALU_CTRL		=>	ALU_CONTROL_in,
		ALU_DIV_RST		=>	ALU_DIV_RST_in,
		ALU_DIV_VALID	=>	ALU_DIV_VALID_in,
		DATAPATH_CTRL  	=>	CTRL_in,
		FWD_CTRL_S     	=>  FWD_CTRL_in,
		NPC1_EN			=>	NPC1_EN_in,
		IR_EN			=>	IR_LATCH_EN_i,
		PC_EN			=>  PC_LATCH_EN_i);

    -- purpose: Instruction Register Process
    -- type   : sequential
    -- inputs : Clk, Rst, IRam_DOut, IR_LATCH_EN_i
    -- outputs: IR_IN_i
    IR_P: process (Clk, Rst)
    begin  -- process IR_P
      if Rst = '1' then                 -- asynchronous reset (active low)
        IR <= (others => '0');
      elsif Clk'event and Clk = '1' then  -- rising clock edge
        if (IR_LATCH_EN_i = '1') then
          IR <= IRAM_DOUT;
        end if;
      end if;
    end process IR_P;

    -- purpose: Program Counter Process
    -- type   : sequential
    -- inputs : Clk, Rst, PC_BUS
    -- outputs: IRam_Addr
    PC_P: process (Clk, Rst)
    begin  -- process PC_P
      if Rst = '1' then                 -- asynchronous reset (active low)
        PC <= (others => '0');
      elsif Clk'event and Clk = '1' then  -- rising clock edge
        if (PC_LATCH_EN_i = '1') then
          PC <= PC_BUS; -- shift by 2 as PC is incremented by 4 but memory increments by 1
        end if;
      end if;
    end process PC_P;

	-- additional connections
	IRAM_ADDR <= "00"&PC(INSTRUCTION_WIDTH-1 downto 2);
	DATAPATH_CTRL_13 <= CTRL_in(13);
	DATAPATH_CTRL_14 <= CTRL_in(14);
	DATAPATH_CTRL_15 <= CTRL_in(15);
    
end dlx_rtl;
