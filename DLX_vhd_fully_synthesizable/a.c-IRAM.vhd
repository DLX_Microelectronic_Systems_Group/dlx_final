library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use std.textio.all;
use ieee.std_logic_textio.all;


-- Instruction memory for DLX
-- Memory filled by a process which reads from a file

entity IRAM is
  generic (
    RAM_DEPTH : integer := 256;
    I_SIZE : integer := 32);
  port (
    Rst  : in  std_logic;
    Addr : in  std_logic_vector(I_SIZE - 1 downto 0);
    Dout : out std_logic_vector(I_SIZE - 1 downto 0)
    );
end IRAM;

architecture IRam_Bhe of IRAM is

  type RAMtype is array (0 to RAM_DEPTH - 1) of integer;

  signal IRAM_mem : RAMtype;

begin  -- IRam_Bhe

  Dout <= conv_std_logic_vector(IRAM_mem(conv_integer(unsigned(Addr))),I_SIZE);

  -- purpose: This process is in charge of filling the Instruction RAM with the firmware
  -- type   : combinational
  -- inputs : Rst
  -- outputs: IRAM_mem

  -- LIST OF PATHS TO ASSEMBLY PROGRAMS -------------------
  -- ../../asm_example/branch/branch.asm.mem
  -- ../../asm_example/dlx_div/dlx_div.asm.mem
  -- ../../asm_example/forwarding_1/forwarding_1.asm.mem
  -- ../../asm_example/forwarding_2/forwarding_2.asm.mem
  -- ../../asm_example/jump/jump.asm.mem
  -- ../../asm_example/jump_and_link/jump_and_link.asm.mem
  -- ../../asm_example/mult0/mult0.asm.mem
  -- ../../asm_example/mult1/mult1.asm.mem
  -- ../../asm_example/shift_and_imm/shift_and_imm.asm.mem
  -- ../../asm_example/test_arithmetic/test_arithmetic.asm.mem
  -- ../../asm_example/bubble_sort/bubble_sort.asm.mem
  -- ../../asm_example/div/div.asm.mem

  FILL_MEM_P: process (Rst)
    file mem_fp: text;
    variable file_line : line;
    variable index : integer := 0;
    variable tmp_data_u : std_logic_vector(I_SIZE-1 downto 0);
  begin  -- process FILL_MEM_P
    if (Rst = '0') then
      file_open(mem_fp,"../../asm_example/mult1/mult1.asm.mem",READ_MODE);
      while (not endfile(mem_fp)) loop
        readline(mem_fp,file_line);
        hread(file_line,tmp_data_u);
        IRAM_mem(index) <= conv_integer(unsigned(tmp_data_u));       
        index := index + 1;
      end loop;
    end if;
  end process FILL_MEM_P;

end IRam_Bhe;
