library IEEE;
use IEEE.std_logic_1164.all;
use WORK.mypackage.all;

entity fd_generic is
	generic (N: integer := N_bit);
	port(D:            In      std_logic_vector (N-1 downto 0);
	     CK, RESET,EN: In      std_logic;
         Q:            Out     std_logic_vector (N-1 downto 0));
end fd_generic;

architecture synch of fd_generic is

begin  -- synchronous d flip flop

  PSYNCH: process (CK, RESET,EN)
  begin  -- process PSYNCH
  if CK'event and CK = '1' then 
     if RESET='1' then
      Q <= (others => '0');
     elsif EN = '1'then
      Q <= D;
     end if;
  end if;
  end process PSYNCH;

end synch;
