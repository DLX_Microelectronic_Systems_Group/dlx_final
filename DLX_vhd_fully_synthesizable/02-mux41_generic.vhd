library IEEE;
use IEEE.std_logic_1164.all; 
use WORK.mypackage.all;

entity MUX41 is
	generic (N:integer:=N_bit);
	port (
		A:		IN	std_logic_vector (N-1 downto 0);
		B:		IN	std_logic_vector (N-1 downto 0);
		C:		IN	std_logic_vector (N-1 downto 0);
		D:		IN	std_logic_vector (N-1 downto 0);
		SEL:	IN	std_logic_vector (1 downto 0);
		Y:		OUT std_logic_vector (N-1 downto 0));
end MUX41;

architecture BEHAVIORAL of MUX41 is
begin

	mux: process(SEL, A, B, C, D)
	begin
	    case SEL is
	        when "00" 	=> Y <= A;
	        when "01" 	=> Y <= B;
	        when "10" 	=> Y <= C;
			when others => Y <= D;
	    end case;
	end process;

end BEHAVIORAL;
