--top entity control unit
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.mypackage.all;

entity DLX_CU is
	port (
		CLK				: IN  std_logic;  													-- Clock
		RST				: IN  std_logic;  													-- Reset:Active-High
		IR			  	: IN  std_logic_vector(INSTRUCTION_WIDTH - 1 downto 0);   			-- Instruction Register
		BRANCH_SEL		: IN std_logic;
		ALU_CTRL	  	: OUT std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);					-- ALU Function select + ALU enable
		ALU_DIV_RST		: OUT std_logic;													-- divisor reset signal
		ALU_DIV_VALID	: IN std_logic;														-- divisor output ready signal
		DATAPATH_CTRL  	: OUT std_logic_vector(CTRL_LENGTH-1 downto 0); 					-- Datapath control signals (ALU excluded)
		FWD_CTRL_S		: OUT std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
		NPC1_EN			: OUT std_logic;
		IR_EN			: OUT std_logic;
		PC_EN			: OUT std_logic);
end DLX_CU;

architecture DLX_CU_HW of DLX_CU is                                

signal CTRL_S1 : std_logic_vector(CU_CTRL_LENGTH-1 downto 0);	-- 25
signal CTRL_S2 : std_logic_vector(CU_CTRL_LENGTH-3 downto 0);	-- 23
signal CTRL_S3 : std_logic_vector(CU_CTRL_LENGTH-10 downto 0);	-- 16
signal CTRL_S4 : std_logic_vector(CU_CTRL_LENGTH-16 downto 0);	-- 10
signal CTRL_S5 : std_logic_vector(CU_CTRL_LENGTH-21 downto 0);	-- 5

signal ALU_CTRL_S1 : std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);
signal ALU_CTRL_S2 : std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);
signal ALU_CTRL_S3 : std_logic_vector(ALU_CTRL_LENGTH-1 downto 0);

-- divisor signals

signal ALU_DIV_RST_S1, ALU_DIV_RST_S2 : std_logic;		-- the divisor is reset during the DECODE stage
signal DIV_STALL, DIV_STALL2 : std_logic;	-- stall signal for division operations
signal PC_EN_in, ALU_DIV_RST_in, ALU_DIV_RST_in0 : std_logic;

-- forwarding control signals
signal FWD_CTRL_S2 : std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
signal FWD_CTRL_S3 : std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
signal FWD_CTRL_S4 : std_logic_vector(2 downto 0);

-- instruction fields and flags used for forwarding check
signal NO_FWD, R_TYPE, I_TYPE_STORE, I_TYPE_LOAD, I_TYPE_LOAD_1, I_TYPE_LOAD_2, I_TYPE_STORE_1, I_TYPE_STORE_2: std_logic;
signal RS1		: std_logic_vector(4 downto 0);
signal RS2		: std_logic_vector(4 downto 0);
signal RD		: std_logic_vector(4 downto 0);
signal RD_1		: std_logic_vector(4 downto 0);
signal RD_2		: std_logic_vector(4 downto 0);

signal NOT_STALL, RECEIVED_STALL, STALL_PREV_CYCLE : std_logic;

begin  -- dlx_cu_rtl

	-- process that propagates the control word through the stages
	CW_PIPE : process (CLK, RST)
	begin
		if RST = '1' then
			CTRL_S2 <= "00000000000000000000001"; 	-- reset RF
			CTRL_S3 <= (others => '0');
			CTRL_S4 <= "0000000001";				-- reset data memory
			CTRL_S5 <= (others => '0');
			ALU_CTRL_S2 <= (others => '0');
			ALU_CTRL_S3 <= (others => '0');
			FWD_CTRL_S3 <= (others => '0');
			FWD_CTRL_S4 <= (others => '0');
			RECEIVED_STALL <= '0';
		elsif (CLK' event and CLK = '1') then
			if (DIV_STALL2 = '1' and ALU_DIV_RST_in = '1') then
				CTRL_S5 		<= CTRL_S4(CU_CTRL_LENGTH-16 downto 5);
			elsif (DIV_STALL2 = '1' and ALU_DIV_RST_in = '0') then
				-- don't propagate anything
			elsif (NOT_STALL = '1' or RECEIVED_STALL = '1') and BRANCH_SEL = '0' then -- normal conditions
				CTRL_S2 		<= CTRL_S1(CU_CTRL_LENGTH-1 downto 2);
				ALU_CTRL_S2 	<= ALU_CTRL_S1;
				FWD_CTRL_S3 	<= FWD_CTRL_S2;
				RECEIVED_STALL 	<= '0';
				CTRL_S3 		<= CTRL_S2(CU_CTRL_LENGTH-3 downto 7);
				CTRL_S4 		<= CTRL_S3(CU_CTRL_LENGTH-10 downto 6);
				CTRL_S5 		<= CTRL_S4(CU_CTRL_LENGTH-16 downto 5);
				ALU_CTRL_S3 	<= ALU_CTRL_S2;
				FWD_CTRL_S4 	<= FWD_CTRL_S3(FWD_CTRL_LENGTH-1 downto FWD_CTRL_LENGTH-3);
			elsif BRANCH_SEL = '1' then								-- branch taken or jump, flush pipeline
				CTRL_S2 		<= "00000010000001000100000";		-- disable RF, enable EN1
				CTRL_S3 		<= "0000001000000100";				-- enable EN2
				CTRL_S4 		<= "0000001000";					-- enable EN3
				CTRL_S5 		<= CTRL_S4(CU_CTRL_LENGTH-16 downto 5);
				ALU_CTRL_S2 	<= (others => '0');
				ALU_CTRL_S3 	<= (others => '0');
				FWD_CTRL_S3 	<= (others => '0');				
				FWD_CTRL_S4 	<= (others => '0');
			elsif (NOT_STALL = '0')	then							-- stall pipeline
				CTRL_S2 		<= (others => '0');
				ALU_CTRL_S2 	<= (others => '0');
				FWD_CTRL_S3 	<= (others => '0');
				RECEIVED_STALL 	<= '1';
				CTRL_S3 		<= CTRL_S2(CU_CTRL_LENGTH-3 downto 7);
				CTRL_S4 		<= CTRL_S3(CU_CTRL_LENGTH-10 downto 6);
				CTRL_S5 		<= CTRL_S4(CU_CTRL_LENGTH-16 downto 5);
				ALU_CTRL_S3 	<= ALU_CTRL_S2;
				FWD_CTRL_S4 	<= FWD_CTRL_S3(FWD_CTRL_LENGTH-1 downto FWD_CTRL_LENGTH-3);
			end if;
		end if;
	end process;

	-- OUTPUT signal assignment ----------------------------------------------------------------------------------------

	-- stage 1
	NPC1_EN 	<= CTRL_S1(0) and NOT_STALL and (not DIV_STALL);
	IR_EN 		<= CTRL_S1(1) and NOT_STALL and (not DIV_STALL);
	PC_EN_in	<= NOT_STALL and (not DIV_STALL);

	PC_EN	 		<= PC_EN_in;
	ALU_DIV_RST 	<= ALU_DIV_RST_in;

	-- stage 2
	DATAPATH_CTRL(0) <= CTRL_S2(0);	
	DATAPATH_CTRL(1) <= CTRL_S2(1) or CTRL_S5(3);	-- RF_EN on decode or write back stage	
	DATAPATH_CTRL(2) <= CTRL_S2(2);
	DATAPATH_CTRL(3) <= CTRL_S2(3);
	DATAPATH_CTRL(4) <= CTRL_S2(4);
	DATAPATH_CTRL(5) <= CTRL_S2(5) and (not DIV_STALL2); -- EN1
	DATAPATH_CTRL(6) <= CTRL_S2(6);

	-- stage 3
	DATAPATH_CTRL(7) <= CTRL_S3(0);
	DATAPATH_CTRL(8) <= CTRL_S3(1);
	DATAPATH_CTRL(9) <= CTRL_S3(2) and (not DIV_STALL2); -- EN2
	DATAPATH_CTRL(10) <= CTRL_S3(3);
	DATAPATH_CTRL(11) <= CTRL_S3(4);
	DATAPATH_CTRL(12) <= CTRL_S3(5);

	-- stage 4
	DATAPATH_CTRL(13) <= CTRL_S4(0);
	DATAPATH_CTRL(14) <= CTRL_S4(1) and (not CLK);
	DATAPATH_CTRL(15) <= CTRL_S4(2);
	DATAPATH_CTRL(16) <= CTRL_S4(3) and (not DIV_STALL2); -- EN3
	DATAPATH_CTRL(17) <= CTRL_S4(4);

	-- stage 5
	DATAPATH_CTRL(20 downto 18) <= CTRL_S5(2 downto 0);
	DATAPATH_CTRL(21) 			<= CTRL_S5(4);	

	-- ALU
	ALU_CTRL <= ALU_CTRL_S3;

	-- Forwarding logic
	FWD_CTRL_S(FWD_CTRL_LENGTH-4 downto 0) <= FWD_CTRL_S3(FWD_CTRL_LENGTH-4 downto 0);
	FWD_CTRL_S(FWD_CTRL_LENGTH-1 downto FWD_CTRL_LENGTH-3) <= FWD_CTRL_S4;
	--------------------------------------------------------------------------------------------------

	-- process that determines the control word starting from the IW
	CW_PROC : process (CLK, RST, IR)
	begin
		if RST='1' then
			CTRL_S1 <= (others => '0');
		else
			case IR(INSTRUCTION_WIDTH-1 downto INSTRUCTION_WIDTH-OPCODE_LENGTH) is -- check OPCODE field
            	when OPCODE_R_TYPE | OPCODE_F_TYPE =>						-- R-type instrunction, check FUNC field
                	CTRL_S1 <= CU_FETCH or CU_READ_RS1_RS2 or CU_USE_A_AND_B or CU_EN3 or CU_WRITE_RF_ADDRD or CU_OUT_SEL;
                when OPCODE_I_TYPE_ADD | OPCODE_I_TYPE_AND | OPCODE_I_TYPE_OR | OPCODE_I_TYPE_SGE | OPCODE_I_TYPE_SLE | OPCODE_I_TYPE_SLL | OPCODE_I_TYPE_SNE | OPCODE_I_TYPE_SRL | OPCODE_I_TYPE_SUB | OPCODE_I_TYPE_XOR | OPCODE_I_TYPE_SRA | OPCODE_I_TYPE_SEQ | OPCODE_I_TYPE_SLT | OPCODE_I_TYPE_SGT =>
                	CTRL_S1 <= CU_FETCH or CU_READ_RS1 or CU_RD_SEL or CU_READ_IMM or CU_USE_A_AND_IMM or CU_EN3 or CU_WRITE_RF_ADDRD or CU_OUT_SEL;
                when OPCODE_I_TYPE_SW =>
                	CTRL_S1 <= CU_FETCH or CU_READ_RS1_RS2 or CU_READ_IMM or CU_USE_A_AND_IMM or CU_WRITE_MEM;
                when OPCODE_I_TYPE_LW =>
                	CTRL_S1 <= CU_FETCH or CU_READ_RS1 or CU_RD_SEL or CU_READ_IMM or CU_USE_A_AND_IMM or CU_READ_MEM or CU_WRITE_RF_OUT;
                when OPCODE_I_TYPE_BEQZ =>
                	CTRL_S1 <= CU_FETCH or CU_READ_RS1 or CU_READ_IMM or CU_USE_NPC_AND_IMM or CU_BRANCH_EN;
                when OPCODE_I_TYPE_BNEZ =>
                	CTRL_S1 <= CU_FETCH or CU_READ_RS1 or CU_READ_IMM or CU_USE_NPC_AND_IMM or CU_BRANCH_EN or CU_BRANCH_NZ;
                when OPCODE_J_TYPE_J =>
                	CTRL_S1 <= CU_FETCH or CU_READ_IMM26 or CU_USE_NPC_AND_IMM or CU_FORCE_JUMP or CU_EN3;
                when OPCODE_J_TYPE_JAL =>
                	CTRL_S1 <= CU_FETCH or CU_READ_IMM26 or CU_USE_NPC_AND_IMM or CU_FORCE_JUMP or CU_EN3 or CU_WRITE_RF_ADD31 or CU_WRITE_RF_PC;
                when OPCODE_NOP =>
                	CTRL_S1 <= CU_FETCH;
                when others =>
                	CTRL_S1 <= CU_FETCH;
			end case;
		end if;
	end process;
    
    -- process that generates the ALU_FUNC codes starting from the IW
    ALU_FUNC_GEN: process (CLK, RST, IR)
    begin
		if RST='1' then
			ALU_CTRL_S1 <= (others => '0');
		else
			case IR(INSTRUCTION_WIDTH-1 downto INSTRUCTION_WIDTH-OPCODE_LENGTH) is -- check OPCODE field    	
    			when OPCODE_I_TYPE_ADD | OPCODE_I_TYPE_SW | OPCODE_I_TYPE_LW | OPCODE_I_TYPE_BEQZ |OPCODE_I_TYPE_BNEZ | OPCODE_J_TYPE_J | OPCODE_J_TYPE_JAL	
                						=> ALU_CTRL_S1 <= '1' & ALU_ADD;
                when OPCODE_I_TYPE_AND 	=> ALU_CTRL_S1 <= '1' & ALU_AND;
                when OPCODE_I_TYPE_OR	=> ALU_CTRL_S1 <= '1' & ALU_OR;
                when OPCODE_I_TYPE_SGE 	=> ALU_CTRL_S1 <= '1' & ALU_SGE;
                when OPCODE_I_TYPE_SLE 	=> ALU_CTRL_S1 <= '1' & ALU_SLE;
                when OPCODE_I_TYPE_SLL	=> ALU_CTRL_S1 <= '1' & ALU_SHL;
                when OPCODE_I_TYPE_SNE	=> ALU_CTRL_S1 <= '1' & ALU_SNE;
                when OPCODE_I_TYPE_SRL	=> ALU_CTRL_S1 <= '1' & ALU_SHRL;
                when OPCODE_I_TYPE_SUB 	=> ALU_CTRL_S1 <= '1' & ALU_SUB;
                when OPCODE_I_TYPE_XOR 	=> ALU_CTRL_S1 <= '1' & ALU_XOR;
				when OPCODE_I_TYPE_SRA	=> ALU_CTRL_S1 <= '1' & ALU_SHRA;
				when OPCODE_I_TYPE_SEQ	=> ALU_CTRL_S1 <= '1' & ALU_SEQ;
				when OPCODE_I_TYPE_SLT	=> ALU_CTRL_S1 <= '1' & ALU_SLT;
				when OPCODE_I_TYPE_SGT	=> ALU_CTRL_S1 <= '1' & ALU_SGT;
               	when OPCODE_R_TYPE | OPCODE_F_TYPE =>	-- check FUNC field
                	case IR(FUNC_LENGTH-1 downto 0) is
                    	when FUNC_ADD	=> ALU_CTRL_S1 <= '1' & ALU_ADD;
                        when FUNC_AND	=> ALU_CTRL_S1 <= '1' & ALU_AND;
                        when FUNC_OR	=> ALU_CTRL_S1 <= '1' & ALU_OR;
                        when FUNC_SGE	=> ALU_CTRL_S1 <= '1' & ALU_SGE;
                        when FUNC_SLE	=> ALU_CTRL_S1 <= '1' & ALU_SLE;
                        when FUNC_SLL	=> ALU_CTRL_S1 <= '1' & ALU_SHL;
                        when FUNC_SNE	=> ALU_CTRL_S1 <= '1' & ALU_SNE;
                        when FUNC_SRL	=> ALU_CTRL_S1 <= '1' & ALU_SHRL;
                        when FUNC_SUB	=> ALU_CTRL_S1 <= '1' & ALU_SUB;
                        when FUNC_XOR	=> ALU_CTRL_S1 <= '1' & ALU_XOR;
						when FUNC_SRA 	=> ALU_CTRL_S1 <= '1' & ALU_SHRA;
						when FUNC_SEQ	=> ALU_CTRL_S1 <= '1' & ALU_SEQ;
						when FUNC_SLT 	=> ALU_CTRL_S1 <= '1' & ALU_SLT;
						when FUNC_SGT	=> ALU_CTRL_S1 <= '1' & ALU_SGT;
						when FUNC_MULT	=> ALU_CTRL_S1 <= '1' & ALU_MUL;
						when FUNC_DIV	=> ALU_CTRL_S1 <= '1' & ALU_DIV;
                        when others		=> ALU_CTRL_S1 <= (others =>'0');
                    end case;
                when others 			=> ALU_CTRL_S1 <= (others =>'0');
            end case;
        end if;
    end process;

	-- DIVISION STALL LOGIC --------------------------------------------------------------------------------------------------------

	division_stall_logic: process (CLK, RST)
    begin
		if (RST = '1') then
			DIV_STALL <= '0';
			DIV_STALL2 <= '0';			
			ALU_DIV_RST_in <= '0';
			ALU_DIV_RST_in0 <= '0';
		elsif (CLK' event and CLK='1') then
			DIV_STALL2 <= DIV_STALL;
			ALU_DIV_RST_in <= ALU_DIV_RST_in0;
			if (IR(INSTRUCTION_WIDTH-1 downto INSTRUCTION_WIDTH-OPCODE_LENGTH) = OPCODE_F_TYPE and IR(FUNC_LENGTH-1 downto 0) = FUNC_DIV and DIV_STALL = '0' and ALU_DIV_RST_in = '0' and PC_EN_in = '1') then
				DIV_STALL <= '1';		-- stall pipeline
				ALU_DIV_RST_in0 <= '1';	-- reset divisor during the first execution cycle
			elsif ALU_DIV_RST_in = '1' then
				ALU_DIV_RST_in0 <= '0';	
				ALU_DIV_RST_in <= '0';
			end if;
			if (ALU_DIV_VALID = '1' and DIV_STALL = '1') then
				DIV_STALL <= '0';
			end if;
		end if;
	end process;
	-- FORWARDING LOGIC ---------------------------------------------------------------------------------------------------------------------
			
	-- extract instruction source operand and destination operand
	-- put destination operand in a 3 stage pipeline so that it can be used for forwarding check
	-- RS1, RS2, RD (current instruction (i) fields)
	-- RD_1 		(i-1) instruction destination register
	-- RD_2			(i-2) instruction destination register
	field_extraction: process (CLK, RST, IR)
	variable current_opcode : std_logic_vector(OPCODE_LENGTH-1 downto 0);
    begin
		if RST='1' then
			RS1 <= (others => '0');
			RS2 <= (others => '0');
			RD 	<= (others => '0');
			NO_FWD <= '1';
			R_TYPE <= '0';
			I_TYPE_STORE <= '0';
			I_TYPE_LOAD  <= '0';
		else
			current_opcode := IR(INSTRUCTION_WIDTH-1 downto INSTRUCTION_WIDTH-OPCODE_LENGTH);
			if (current_opcode = OPCODE_J_TYPE_J or current_opcode = OPCODE_J_TYPE_JAL or current_opcode = OPCODE_NOP or current_opcode = OPCODE_I_TYPE_BEQZ or current_opcode = OPCODE_I_TYPE_BNEZ or IR =  "00000000000000000000000000000000" or IR = "10000000000000000000000000000000" or BRANCH_SEL = '1') then
				-- No need for forwarding, raise flag
				NO_FWD <= '1';
				-- assign 31 (not allowed value) to their instruction fields so that the following instruction won't consider them
				RS1 <= (others => '1');
				RS2 <= (others => '1');
				RD	<= (others => '1');
				R_TYPE <= '0';
				I_TYPE_STORE <= '0';
				I_TYPE_LOAD  <= '0';
			elsif (current_opcode = OPCODE_R_TYPE or current_opcode = OPCODE_F_TYPE) then
				-- R-type or F-type instruction
				RS1 <= IR(25 downto 21);
				RS2 <= IR(20 downto 16);
				RD	<= IR(15 downto 11);
				NO_FWD <= '0';
				R_TYPE <= '1';
				I_TYPE_STORE <= '0';
				I_TYPE_LOAD  <= '0';
			else
				-- I-type instruction
				RS1 <= IR(25 downto 21);
				RD	<= IR(20 downto 16);
				NO_FWD <= '0';
				R_TYPE <= '0';
				if (current_opcode = OPCODE_I_TYPE_SW) then 
					-- I-type STORE instruction
					I_TYPE_STORE <= '1';
					I_TYPE_LOAD  <= '0';
				elsif (current_opcode = OPCODE_I_TYPE_LW) then
					-- I-type LOAD instruction
					I_TYPE_STORE <= '0';
					I_TYPE_LOAD  <= '1';
				else 
					-- I-type not LOAD nor STORE instruction
					I_TYPE_STORE <= '0';
					I_TYPE_LOAD  <= '0';
				end if;
			end if;
		end if;
	end process;
			
	field_pipeline: process (CLK, RST)
	begin
		if RST = '1' then 
			RD_1 <= (others => '1');
			RD_2 <= (others => '1');
			I_TYPE_LOAD_1  <= '0';
			I_TYPE_LOAD_2  <= '0';
			I_TYPE_STORE_1 <= '0';
			I_TYPE_STORE_2 <= '0';
		elsif (CLK' event and CLK = '1' and PC_EN_in = '1') then
			if NOT_STALL = '1' then			
				RD_1 <= RD;
				I_TYPE_LOAD_1	<= I_TYPE_LOAD;
				I_TYPE_STORE_1 	<= I_TYPE_STORE;				
			end if;
			RD_2 <= RD_1;
			I_TYPE_LOAD_2 	<= I_TYPE_LOAD_1;
			I_TYPE_STORE_2 	<= I_TYPE_STORE_1; 
		end if;
	end process;

	-- stall logic
	-- OUTPUTS: NOT_STALL
	-- INPUTS: RS1, RS2 ,RD ,NO_FWD ,R_TYPE ,I_TYPE_STORE ,I_TYPE_LOAD ,RD_1 ,RD_2 ,I_TYPE_LOAD_1 ,I_TYPE_LOAD_2
	STALL_GEN: process (CLK, RST)
    begin
		if RST = '1' or NO_FWD = '1' then
			NOT_STALL <= '1';
			STALL_PREV_CYCLE <= '0';			-- signals that a stall occurred in the previous cycle
		elsif (CLK' event and CLK = '0') then	-- falling clock edge, check for stall condition
			if (RS1 = RD_1 or (RS2 = RD_1 and R_TYPE = '1')) and I_TYPE_LOAD_1 = '1' and STALL_PREV_CYCLE = '0' then 
				NOT_STALL <= '0';
			elsif RS1 = RD_2 and I_TYPE_STORE = '0' and I_TYPE_LOAD_2 = '1' and STALL_PREV_CYCLE = '0' then
				NOT_STALL <= '0';
			elsif R_TYPE = '1' and RS2 = RD_2 and I_TYPE_LOAD_2 = '1' and STALL_PREV_CYCLE = '0' then
				NOT_STALL <= '0';
			end if;
			if RECEIVED_STALL = '1' then
				NOT_STALL <= '1';
				STALL_PREV_CYCLE <= '1';
			end if;
			if STALL_PREV_CYCLE = '1' then
				STALL_PREV_CYCLE <= '0';
			end if; 
		end if; 
	end process;

	-- forwarding logic process
	-- OUTPUTS: STALL, FWD_CTRL_S2
	-- INPUTS: RS1, RS2 ,RD ,NO_FWD ,R_TYPE ,I_TYPE_STORE ,I_TYPE_LOAD ,RD_1 ,RD_2 ,I_TYPE_LOAD_1 ,I_TYPE_LOAD_2
	FWD_GEN: process (RST, CLK)
	    variable FWD_var : std_logic_vector(FWD_CTRL_LENGTH-1 downto 0);
    begin
		if (CLK' event and CLK = '1') then
			if RST = '1' or NO_FWD = '1' then
				FWD_CTRL_S2 <= (others => '0');
			else
				FWD_var := (others => '0');
			-- check for data dependencies
			-- RS1 check 
			    if RS1 = RD_1 and  I_TYPE_STORE_1 = '0' then
			        if I_TYPE_LOAD_1 = '0' then										-- previous operation is not a LOAD nor a STORE
			            FWD_var := FWD_var or CU_FWD_A or CU_FWD_ALU_SEL_A_ALUOUT1;	-- forward ALU out 1 to A operand
			        elsif I_TYPE_LOAD_1 = '1' then          						-- previous operation is a LOAD
						FWD_var := FWD_var or CU_FWD_A or CU_FWD_ALU_SEL_A_MEMOUT1; -- forward memory out to ALU A operand
			        end if;
			    elsif RS1 = RD_2 then
			        if I_TYPE_LOAD_2 = '0' and I_TYPE_STORE_2 = '0' then			-- i-2 operation is not a LOAD nor a STORE
			            FWD_var := FWD_var or CU_FWD_A or CU_FWD_ALU_SEL_A_ALUOUT2; -- forward ALU out 2 to A operand
			        elsif I_TYPE_STORE = '0' and I_TYPE_LOAD_2 = '1' then           -- i-2 operation is a LOAD
						FWD_var := FWD_var or CU_FWD_A or CU_FWD_ALU_SEL_A_MEMOUT2; -- forward  memory out 2 to ALU A operand
		            end if;
		        end if;
		        -- RS2 check: only for R-TYPE instructions
		        if R_TYPE = '1' then																								
		            if RS2 = RD_1 and I_TYPE_STORE_1 = '0' then
		                if I_TYPE_LOAD_1 = '0' then										-- previous operation is not a LOAD	nor a STORE			
		                	FWD_var := FWD_var or CU_FWD_B or CU_FWD_ALU_SEL_B_ALUOUT1;	-- forward ALU out 1 to B operand
		                elsif I_TYPE_LOAD_1 = '1' then 									-- previous operation is a LOAD
							FWD_var := FWD_var or CU_FWD_B or CU_FWD_ALU_SEL_B_MEMOUT1; -- forward memory out to ALU B operand			
		                end if;
					elsif RS2 = RD_2 then
		                if I_TYPE_LOAD_2 = '0' and I_TYPE_STORE_2 = '0' then			-- i-2 operation is not a LOAD nor a STORE
		                	FWD_var := FWD_var or CU_FWD_B or CU_FWD_ALU_SEL_B_ALUOUT2;	-- forward ALU out 2 to B operand
		                elsif I_TYPE_LOAD_2 = '1' then 									-- i-2 operation is a LOAD
		                	FWD_var := FWD_var or CU_FWD_B or CU_FWD_ALU_SEL_B_MEMOUT2; -- forward  memory out 2 to ALU B operand
		                end if;
		            end if;	            
		        end if;
		        -- RD check: only for STORE and LOAD instructions
		        if RD = RD_1 and I_TYPE_STORE = '1' and I_TYPE_LOAD_1 = '0' and I_TYPE_STORE_1 = '0' then
		        	FWD_var := FWD_var or CU_FWD_DATAIN or CU_FWD_MEM_SEL_ALUOUT2;	-- forward ALU out 2 to mem datain
		        elsif RD = RD_2 and I_TYPE_STORE = '1' and I_TYPE_LOAD_1 = '0' and I_TYPE_STORE_2 = '0' then
		        	FWD_var := FWD_var or CU_FWD_DATAIN or CU_FWD_MEM_SEL_ALUOUT3;	-- forward ALU out 3 to mem datain
		        end if;
				if RD = RD_1 and I_TYPE_STORE = '1' and I_TYPE_LOAD_1 = '1' then
					FWD_var := FWD_var or CU_FWD_DATAIN	or CU_FWD_MEM_SEL_MEMOUT1;	-- forward MEM out 1 to MEM datain
				elsif RD = RD_2 and I_TYPE_STORE = '1' and I_TYPE_LOAD_1 = '1' then
					FWD_var := FWD_var or CU_FWD_DATAIN or CU_FWD_MEM_SEL_MEMOUT2;	-- forward MEM out 2 to MEM datain
				end if;
		        FWD_CTRL_S2 <= FWD_var;
			end if;
	    end if;
	end process;
end dlx_cu_hw;
